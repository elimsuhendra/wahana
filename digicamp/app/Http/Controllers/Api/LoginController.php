<?php namespace digipos\Http\Controllers\Api;

use Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use digipos\models\Member_user;
use digipos\models\User;

class LoginController extends Controller {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function login(request $request){
		$email 		= $request->email;
		$password 	= $request->password;
		
		$auth 		= auth()->guard($this->guard);
		$data = [
			'email' => $email,
			'password' => $password,
			'status' => 'y'
		];
		$auth = $auth->attempt($data);
		if($auth){
			$token 	= $this->buildToken($auth);
			$user 	= auth()->guard($this->guard)->user();
			$res['user'] 	= ['status' => 'success','text' => 'Success login','token' => $token,'user' => $user];
		}else{
			$res['user'] 	= ['status' => 'fail','text' => 'Email or password is incorrect'];
		}
		return response()->json($res);
	}

	public function register(request $request){
		$name 		= $request->name;
		$email 		= $request->email;
		$password 	= $request->password;
		
		$checkemail = Customer::where('email',$email)->first();
		if($checkemail){
			$res['user'] = ['status' => 'fail','text' => 'Email already registered'];
		}else{
			$user 			= new Customer;
			$user->name 	= $name;
			$user->email 	= $email;
			$user->password = Hash::make($password);
			$user->status 	= 'y';
			$user->save();

			$auth 		= auth()->guard($this->guard);
			$data = [
				'email' => $email,
				'password' => $password,
				'status' => 'y'
			];
			$auth 			= $auth->attempt($data);
			$token 			= $this->buildToken($auth);
			$res['user'] 	= ['status' => 'success','text' => 'Success register','token' => $token,'user' => $user];
		}
		return response()->json($res);
	}

	public function buildToken(){
		$user 	= auth()->guard($this->guard)->user();
		$token 	= JWTAuth::fromUser($user);
		return $token;
	}

	public function forgot_password($token = ""){
		$this->data["token"] = $token;
		$reset = Member_user::where('reset_password_token', $token)->first();
		dd($reset);
		if(count($reset) > 0) return $this->render_view('reset');
		else redirect()->to('/');
	}

	public function reset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);

        $member_user = Member_user::where('reset_password_token', $request->token)->first();

        if(count($member_user) > 0){
        	$member_user = Member_user::find($member_user->id);
        	$member_user->password = Hash::make($request->password);
        	$member_user->reset_password_token = "";
        	$member_user->save();
     	}
        
        return redirect()->to('/_admin');
    }
}
