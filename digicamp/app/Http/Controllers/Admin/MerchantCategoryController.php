<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Invoicelog;
use digipos\models\Msmerchant;
use digipos\models\Member;
use digipos\models\Memberuser;
use digipos\models\Membertype;
use digipos\models\Member_user;
use digipos\models\Store;
use digipos\models\Useraccess;
use digipos\models\Merchant_category;

// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use File;
use digipos\Libraries\Alert;
use digipos\Libraries\Email;
use Illuminate\Http\Request;

class MerchantCategoryController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Merchant Category';
		$this->root_link		= 'merchant-category';
		$this->bulk_action_data = [1];
		$this->model			= new Merchant_category;
		$this->bulk_action		= true;
		$this->image_path 		= 'components/admin/image/merchant_category/';
		$this->data['image_path'] = $this->image_path;
		// $this->hide_edit_button	= true;
	}

	public function index(){
		$this->field = [
			[
				'name' 		=> 'category_name',
				'label' 	=> 'Category Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model;
		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'category_name',
				'label' => 'Category Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
				'form_class' => 'col-md-12',
			],
			[
				'name' => 'image',
				'label' => 'Image',
				'type' => 'file',
				'file_opt' => ['path' => $this->image_path],
				'upload_type' => 'single-image',
				'form_class' => 'col-md-6',
				'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
				'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: 1631 x 927 px'
			],
			[
				'name' 		=> 'headline',
				'label' 	=> 'Headline',
				'sorting' 	=> 'y',
				'search' => 'select',
				'data' => ['Headline'],
				'type' => 'checkbox',
				'content' => 'Headline',
				'value'	=> [],
			]
		];
		return $field;
	}

	public function field_edit($id){
		$category = $this->model->find($id);
		$value = [];
		if($category->headline == 'y'){
			$value[0] = 'y';
		}

		$field = [
			[
				'name' => 'category_name',
				'label' => 'Category Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			],
			[
				'name' => 'image',
				'label' => 'Image',
				'type' => 'file',
				'file_opt' => ['path' => $this->image_path],
				'upload_type' => 'single-image',
				'form_class' => 'col-md-6 pad-left',
				'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
				'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: 1631 x 927 px'
			],
			[
				'name' 		=> 'headline',
				'label' 	=> 'Headline',
				'sorting' 	=> 'y',
				'search' => 'select',
				'data' => ['Headline'],
				'type' => 'checkbox',
				'content' => 'Headline',
				'value'	=> $value
			]
		];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	public function store(Request $request){
		$this->validate($request,[
				'category_name' => 'required|unique:merchant_category,category_name',
			]);
		
		$this->model->category_name	= $request->category_name;
		
		$this->model->status 		= 'y';
		$this->model->updated_by 	= auth()->guard($this->guard)->user()->id;

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path, 'width' => '1631', 'height' => '927']
					];
			$image = $this->build_image($data);
			$this->model->image = $image;
		}
		$headline 	= $request->headline;
		$hl = 'n';
		if($headline == null){
			$hl = 'n';
		}else{
			if($headline[0] == '0'){
				$hl = 'y';
			}
		}

		$this->model->headline = $hl;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully create new merchant category');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model = $this->model->find($id);
		$this->field = $this->field_edit($id);
		return $this->build('view');
	}

	public function edit($id){
		$this->model = $this->model->find($id);
		$this->field = $this->field_edit($id);
		return $this->build('edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
				'category_name' => 'required|unique:msmerchant,merchant_name',
			]);
		
		$this->model = $this->model->find($id);
		$this->model->category_name	= $request->category_name;
		
		$this->model->status 		= 'y';
		$this->model->updated_by 		= auth()->guard($this->guard)->user()->id;

		if($request->input('remove-single-image-merchant_category') == 'y'){
			if($this->model->image != NULL){
				File::delete($this->image_path.$this->model->image);
				$this->model->image = '';
			}
		}

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path, 'width' => '1631', 'height' => '927']
					];
			$image = $this->build_image($data);
			$this->model->image = $image;
		}

		$headline 	= $request->headline;
		$hl = 'n';
		if($headline == null){
			$hl = 'n';
		}else{
			if($headline[0] == '0'){
				$hl = 'y';
			}
		}

		$this->model->headline = $hl;
		// dd($this->model);

		$this->model->updated_at = date("Y-m-d H:i:s");
		$this->model->save();

		Alert::success('Successfully edit merchant category');
		return redirect()->to($this->data['path']);
	}									

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		
		$uc->delete();
		Alert::success('Merchant has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		$store = $this->myStore();

		$list_member_id = Member::whereIn('store_id', $store)->groupBy('user_id')->pluck('user_id')->toArray();
		return $this->build_export($list_member_id);
	}
}
?>