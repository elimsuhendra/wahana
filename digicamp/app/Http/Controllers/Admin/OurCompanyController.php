<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Msmerchant;
use digipos\models\Authmenu;
use digipos\models\Useraccess;
use digipos\models\Store;
use digipos\models\User;
use digipos\models\Our_company;
use digipos\models\Config;
// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use File;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;

class OurCompanyController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Our Company';
		$this->root_link		= 'our_company';
		$this->model			= new Our_company;
		$this->user				= new user;
		// $this->delete_relation	= ['our_company'];
		$this->bulk_action		= true;
		$this->bulk_action_data = [1];
		$this->image_path 		= 'components/admin/image/our_company/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= 'components/both/images/web/';

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	public function index(){
		$this->field = [
			[
				'name' 		=> 'our_company_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->orderBy('id');
		return $this->build('index');
	}

	public function show($id){
		$this->data['data1'] = DB::table('our_company as a')
									->where('a.id',$id)
									->first();
		$this->data["title"] = "View Our Company ".$this->data['data1']->our_company_name;
		return $this->render_view('pages.our_company.view');
	}

	public function edit($id){
		$this->data['data1'] = DB::table('our_company as a')
									->where('a.id',$id)
									->first();
		$this->data["title"] = "View Our Company ".$this->data['data1']->our_company_name;
		return $this->render_view('pages.our_company.edit');
	}

	public function update(Request $request, $id){
		$this->user 					= $this->user->where('email', $request->email_user)->first();
		// dd($this->model->get());
		$this->validate($request,[
				'our_company_name' 	=> 'required|min:5|unique:our_company,our_company_name,'.$id,
				'description' 		=> 'required',
			]);

		$this->model				= $this->model->find($id);
		
		$this->model->our_company_name	= $request->our_company_name;
		$this->model->description		= $request->description;
		$this->model->status 			= 'y';
		$this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		$this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_description : $this->meta_description->value;

		$this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_keyword : $this->meta_keyword->value;
		$this->model->updated_by 		= auth()->guard($this->guard)->user()->id;

		if($request->input('remove-single-image-company_image') == 'y'){
			if($this->model->image != NULL){
				File::delete($this->image_path.$this->model->image);
				$this->model->image = '';
			}
		}

		if ($request->hasFile('company_image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'company_image',
						'file_opt' => ['path' => $this->image_path, 'width' => '120', 'height' => '120']
					];
			$image = $this->build_image($data);
			$this->model->image = $image;
		}

		if($request->input('remove-single-image-company_image_active') == 'y'){
			if($this->model->image_hover != NULL){
				File::delete($this->image_path.$this->model->image);
				$this->model->image_hover = '';
			}
		}

		if ($request->hasFile('company_image_active')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'company_image_active',
						'file_opt' => ['path' => $this->image_path, 'width' => '120', 'height' => '120']
					];
			$image = $this->build_image($data);
			$this->model->image_hover = $image;
		}
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully edit Our Company');
		return redirect()->to($this->data['path']);
	}	

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function export(){
		return $this->build_export();
	}
}

?>