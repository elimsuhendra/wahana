<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\request;

use digipos\models\Our_company;
use digipos\models\Our_service;
use digipos\models\News;
use digipos\models\Banner;
use digipos\models\Card_category;
use digipos\models\Apply;
use digipos\models\Config;
use digipos\models\Social_media;

use digipos\models\Client;
use digipos\models\Client_card;
use digipos\models\Merchant_category;
use digipos\models\Msmerchant;
use digipos\models\Outlet;
use digipos\models\Promo;
use digipos\models\Merchant_log;
use DB;


class PagesController extends ShukakuController {

	public function __construct(){
		parent::__construct();

		$this->menu = $this->data['path'][0];
		$this->data['menu'] 		= $this->menu;
	}

	public function sc_live(request $request){
		$search = $request->search;
		$merchant = Merchant_category::join('msmerchant', 'merchant_category.id', 'msmerchant.id_merchant_category')->where([['merchant_category.status', 'y'],['merchant_category.category_name', 'LIKE', '%'.$search.'%']])->select('merchant_category.category_name')->distinct()->get();

		if($merchant != NULL){
			$result = $merchant; 
		} else{
			$result['status'] = "No Result";
		}

		return response()->json(['result' => $result]);
	}


	public function company(){
		$this->data['content'] = Our_company::where('status', 'y')->get();

		$meta = Our_company::select('meta_title', 'meta_description', 'meta_keyword')->first(); 

		$this->data['met_title'] = $meta->meta_title != NULL ? $meta->meta_title : $this->data['web_title'];
		$this->data['met_keywords'] = $meta->meta_keyword != NULL ? $meta->meta_keyword : $this->data['web_title'];
		$this->data['met_description'] = $meta->meta_description != NULL ? $meta->meta_description : $this->data['web_title'];
		return $this->render_view('pages.pages.our_company');
	}

	public function merchant(request $request){
		$category = $request->category;
		$location = $request->location;

		$query = Merchant_category::join('msmerchant', 'merchant_category.id', 'msmerchant.id_merchant_category')->where([['merchant_category.status', 'y'], ['msmerchant.status', 'y'],['msmerchant.end_date', '>=', date('Y-m-d')]])->select('merchant_category.id', 'merchant_category.category_name');

		$query2 = Msmerchant::where([['status', 'y'],['end_date', '>=', date('Y-m-d')]]);
		if($category != NULL){
			$query->where('merchant_category.category_name' ,'like', '%'.$request->filter.'%');
		} 

		if($location != NULL){
			$query->join('city', 'msmerchant.id_city', 'city.id')->where('city.name', 'like', '%'.$request->filter.'%');
			$query2->join('city', 'msmerchant.id_city', 'city.id')->where('city.name', 'like', '%'.$request->filter.'%');
		}

		$this->data['met_title'] 		= 'Merchant - '.$this->data['web_title'];
		$this->data['met_keywords'] 	= 'Merchant - '.$this->data['web_keywords'];
		$this->data['met_description'] 	= 'Merchant - '.$this->data['web_description'];

		$this->data['merchant'] = $query->distinct()->get();
		$this->data['client'] = $query2->get();
		$this->data['populer'] = Msmerchant::select('msmerchant.merchant_name','msmerchant.slug','msmerchant.id', 'msmerchant.logo', DB::raw('COUNT(*) as counts'))->join('merchant_log','merchant_log.merchant_id', 'msmerchant.id')->groupBy('msmerchant.id')->orderBy('counts', 'desc')->limit(6)->get();

		return $this->render_view('pages.pages.merchant');
	}

	public function merchant_category($slug){
		$category = $slug;

		$query = Merchant_category::join('msmerchant', 'merchant_category.id', 'msmerchant.id_merchant_category')->where([['merchant_category.status', 'y'], ['msmerchant.status', 'y']])->select('merchant_category.id', 'merchant_category.category_name')->where('merchant_category.category_name' ,'like', '%'.$category.'%');

		$query2 = Msmerchant::where('msmerchant.status', 'y');

		$this->data['met_title'] 		= 'Merchant - '.$this->data['web_title'];
		$this->data['met_keywords'] 	= 'Merchant - '.$this->data['web_keywords'];
		$this->data['met_description'] 	= 'Merchant - '.$this->data['web_description'];

		$this->data['merchant'] = $query->distinct()->get();
		$this->data['client'] = $query2->get();
		$this->data['populer'] = Msmerchant::select('msmerchant.merchant_name','msmerchant.slug','msmerchant.id', 'msmerchant.logo', DB::raw('COUNT(*) as counts'))->join('merchant_log','merchant_log.merchant_id', 'msmerchant.id')->groupBy('msmerchant.id')->orderBy('counts', 'desc')->limit(6)->get();

		return $this->render_view('pages.pages.merchant');
	}

	public function filter_merchants(request $request){
		$category = $request->category;
		$location = $request->location;

		$query = Merchant_category::join('msmerchant', 'merchant_category.id', 'msmerchant.id_merchant_category')->where([['merchant_category.status', 'y'], ['msmerchant.status', 'y'],['msmerchant.end_date', '>=', date('Y-m-d')]])->select('merchant_category.id', 'merchant_category.category_name');

		$query2 = Msmerchant::where([['status', 'y'],['end_date', '>=', date('Y-m-d')]]);

		if($category != NULL){
			$query->where('merchant_category.category_name' ,'like', '%'.$request->filter.'%');
		} 

		if($location != NULL){
			$query->join('city', 'msmerchant.id_city', 'city.id')->where('city.name', 'like', '%'.$request->filter.'%');
			$query2->join('city', 'msmerchant.id_city', 'city.id')->where('city.name', 'like', '%'.$request->filter.'%');
		}

		$this->data['met_title'] 		= 'Merchant - '.$this->data['web_title'];
		$this->data['met_keywords'] 	= 'Merchant - '.$this->data['web_keywords'];
		$this->data['met_description'] 	= 'Merchant - '.$this->data['web_description'];

		$this->data['merchant'] = $query->distinct()->get();
		$this->data['client'] = $query2->get();
		$this->data['populer'] = Msmerchant::select('msmerchant.merchant_name','msmerchant.slug','msmerchant.id', 'msmerchant.logo', DB::raw('COUNT(*) as counts'))->join('merchant_log','merchant_log.merchant_id', 'msmerchant.id')->groupBy('msmerchant.id')->orderBy('counts', 'desc')->limit(6)->get();

		return $this->render_view('pages.pages.merchant');
	}

	public function merchant_detail($ids){

		$client = Msmerchant::join('merchant_category', 'msmerchant.id_merchant_category', 'merchant_category.id')->where('msmerchant.slug', $ids)->select('msmerchant.*', 'merchant_category.image', 'merchant_category.id as mc_id')->first();
		// dd($client);
		$get_ip = request()->ip();

		// $get_ip = "66.102.0.0";

		if($get_ip){
			$ip = $get_ip;
	    	$location = \Location::get($ip);

	    	$merchant = Merchant_log::where([['merchant_id', $client->id], ['ip_address', $ip]])->whereDate('created_at', '=', date('Y-m-d'))->count();	

			if($merchant == 0){
				$set_log = new Merchant_log;
				$set_log->merchant_id = $client->id;
				$set_log->ip_address = $ip;
				$set_log->city = $location->cityName;
				$set_log->status = 'y';
				$set_log->updated_by = 1;
				$set_log->save();
			}
		} 
	

		$this->data['met_title'] 		= $client->meta_title != NULL ? $client->meta_title : $this->data['web_title'];
		$this->data['met_keywords'] 	= $client->meta_keywords != NULL ? $client->meta_keywords : $this->data['web_keywords'];
		$this->data['met_description'] 	= $client->meta_description != NULL ? $client->meta_description : $this->data['web_description'];

		$this->data['client'] = $client;
		$this->data['outlet'] = Outlet::where('merchant_id', $client->id)->get();
		$this->data['promo'] = Promo::where('merchant_id', $client->id)->select('id','promo_name', 'promo_slug', 'image')->where([['status', 'y'],['start_date', '<=', date('Y-m-d')],['end_date', '>=', date('Y-m-d')]])->get();
		
		$this->data['related'] = Msmerchant::where([['id_merchant_category', $client->mc_id], ['status', 'y'], ['id', '!=', $client->id]])->get();
		$this->data['populer'] = Msmerchant::select('msmerchant.merchant_name','msmerchant.slug','msmerchant.id', 'msmerchant.logo', DB::raw('COUNT(*) as counts'))->join('merchant_log','merchant_log.merchant_id', 'msmerchant.id')->where('msmerchant.status', 'y')->groupBy('msmerchant.id')->orderBy('counts', 'desc')->limit(6)->get();

		return $this->render_view('pages.pages.detail_merchant');
	}

	public function promo($ids){
		$this->data['promo'] = Promo::where('promo_slug', $ids)->first();

		$this->data['met_title'] 		= 'Promo '.$this->data['web_name'].' - '.$this->data['promo']->promo_name;
		$this->data['met_keywords'] 	= 'Promo '.$this->data['web_name'].' - '.$this->data['promo']->promo_name;
		$this->data['met_description'] 	= 'Promo '.$this->data['web_name'].' - '.$this->data['promo']->description;
		return $this->render_view('pages.pages.promo');
	}

	public function services(){
		$this->data['content'] = Our_service::where('status', 'y')->get();

		$meta = Our_service::select('meta_title', 'meta_description', 'meta_keyword')->first(); 

		$this->data['met_title'] = $meta->meta_title != NULL ? $meta->meta_title : $this->data['web_title'];
		$this->data['met_keywords'] = $meta->meta_keyword != NULL ? $meta->meta_keyword : $this->data['web_title'];
		$this->data['met_description'] = $meta->meta_description != NULL ? $meta->meta_description : $this->data['web_title'];
		return $this->render_view('pages.pages.our_services');
	}

	public function news(){
		$this->data['slide'] = News::where([['status', 'y'], ['image', '!=', '']])->limit(4)->orderBy('id', 'desc')->get();
		$this->data['content'] = News::join('user', 'news.updated_by' , 'user.id')->where('news.status', 'y')->select('news.*', 'user.username')->orderBy('created_at', 'desc')->paginate(4);
		$this->data['popular'] = News::where('status', 'y')->orderBy('count', 'desc')->limit(8)->get();

		$this->data['met_title'] 		= 'News - '.$this->data['web_title'];
		$this->data['met_keywords'] 	= 'News - '.$this->data['web_keywords'];
		$this->data['met_description'] 	= 'News - '.$this->data['web_description'];
		return $this->render_view('pages.pages.news');
	}

	public function detail_news($slug){
		$this->data['content'] = News::join('user', 'news.updated_by' , 'user.id')->where([['news.status', 'y'],['news.slug', $slug]])->select('news.*', 'user.username')->first();
		News::where('slug', $slug)->increment('count');
		$this->data['popular'] = News::where('status', 'y')->orderBy('count', 'desc')->limit(8)->get();

		$this->data['met_title'] = $this->data['content']->meta_title != NULL ? $this->data['content']->meta_title : $this->data['web_title'];
		$this->data['met_keywords'] = $this->data['content']->meta_keyword != NULL ? $this->data['content']->meta_keyword : $this->data['web_title'];
		$this->data['met_description'] = $this->data['content']->meta_description != NULL ? $this->data['content']->meta_description : $this->data['web_title'];
		return $this->render_view('pages.pages.detail_news');
	}

	public function apply(){
		$this->data['client'] = client::where('status', 'y')->get();
		$this->data['content'] = Banner::where('id', '2')->first();
		$this->data['met_title'] 		= 'Apply - '.$this->data['web_title'];
		$this->data['met_keywords'] 	= 'Apply - '.$this->data['web_keywords'];
		$this->data['met_description'] 	= 'Apply - '.$this->data['web_description'];
		return $this->render_view('pages.pages.apply');
	}

	public function apply_sub(request $request){
		$this->validate($request,[
			'ap_name'							=> 'required',
			'ap_telp'							=> 'required|numeric',
			'ap_address'						=> 'required',
			'ap_client'							=> 'required',
		],
		[
			'ap_name.required'					=> 'Nama Wajib diisi',
			'ap_telp.required'					=> 'Nomor telepon wajib diisi',
			'ap_telp.numeric'					=> 'Nomor telepon harus angka',
			'ap_address.required'				=> 'Alamat Wajib diisi',
			'ap_client.required'				=> 'Klien Wajib dipilih',
		]);

		$apply = new Apply;
		$apply->name = $request->ap_name;
		$apply->phone = $request->ap_telp;
		$apply->email = $request->ap_email;
		$apply->address = $request->ap_address;
		$apply->client_id = $request->ap_client;
		$apply->status = 'y';
		$apply->save();

		$email_admin = $this->data['web_email'];
		$get_client = Client::where('id', $request->ap_client)->select('client_name')->first();

		$url      = "front.mail.content";
		$title    = "Apply";
		$subject  = "Apply Customer"; 
		$status   = "admin";
		$to 	  = $email_admin;
		$content  = "   <p>Hi admin,&nbsp;</p>
						<p>ada pelanggan yang melakukan apply dengan website, berikut data pelanggan berikut :</p>
						<p>Nama &nbsp; &nbsp; : ".$request->ap_name."</p>
						<p>Email &nbsp; &nbsp; : ".$request->ap_email."</p>
						<p>No Telp &nbsp;: ".$request->ap_telp."</p>
						<p>Alamat &nbsp; : ".$request->ap_address."</p>
						<p>Klien &nbsp; &nbsp; &nbsp;: ".$get_client->client_name."</p>
					";

		$request->request->add([
								'e_url'     => $url,
								'e_title'   => $title,
								'e_subject' => $subject,
								'e_email'	=> $to,
								'e_status'  => $status,
								'e_content' => $content
								]);

		$this->email($request);

       return redirect()->back()->with('message', 'Success, Thanks For Apply');
	}

	public function contact(){
		$info = ['13', '14', '15', '16'];
		$medsos = ['1', '5', '6'];
		$config = Config::whereIn('id', $info)->get();

		foreach($config as $cg){
			$this->data[$cg->name] = $cg->value;
		}

		$this->data['content'] = Banner::where('id', '5')->first();
		$this->data['medsos'] = Social_media::whereIn('id', $medsos)->get();
		$this->data['met_title'] 		= 'Apply - '.$this->data['web_title'];
		$this->data['met_keywords'] 	= 'Apply - '.$this->data['web_keywords'];
		$this->data['met_description'] 	= 'Apply - '.$this->data['web_description'];
		return $this->render_view('pages.pages.contact');
	}

	public function our_clients(){
		$this->data['active'] 		= ($this->menu == 'our-clients' ? 'menu-active' : '');
		$this->data['client'] 		= Client::join('client_card', 'client.id', 'client_card.id_client')->where('client.status', 'y')->orderBy('client.id', 'desc')->select('client.*')->distinct()->get();
		$this->data['client_card'] 	= json_encode(Client_card::get());

		$this->data['met_title'] 		= 'Our Client - '.$this->data['web_title'];
		$this->data['met_keywords'] 	= 'Our Client - '.$this->data['web_keywords'];
		$this->data['met_description'] 	= 'Our Client - '.$this->data['web_description'];
		return $this->render_view('pages.pages.our_clients');
	}
}
