<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model{
	protected $table 		 	= 'client';
	protected $card   	 	 	= 'digipos\models\Card';

    public function member(){
        return $this->hasMany($this->card,'card_id');
    }
}