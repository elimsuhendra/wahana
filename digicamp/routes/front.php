<?php
//Main route
Route::get('/', 'IndexController@index')->name('cs_home');
Route::post('lock-in', 'AccountController@lock_in');
Route::get('logout', 'AccountController@lock_out');
Route::get('profile', 'AccountController@profile')->name('cs_profile');
Route::get('edit-profile', 'AccountController@edit_profile');
Route::post('edit-profile', 'AccountController@update_profile');
Route::get('change-password', 'AccountController@change_password');
Route::post('change-password', 'AccountController@ch_password');

Route::post('v1/live_search', 'PagesController@sc_live');

Route::post('forgot-password', 'AccountController@forgot_password');
Route::get('forget-password/{token}', 'AccountController@c_forgot_password');
Route::post('forget-password/confirm-password', 'AccountController@ch_forgot_password');

Route::get('cart', 'CheckoutController@cart');
Route::get('contact', 'PagesController@contact');
Route::get('product/{id}', 'ProductController@index');


Route::get('merchants', 'PagesController@merchant');
Route::get('merchants-category/{slug}', 'PagesController@merchant_category');
Route::get('merchants/{slug}', 'PagesController@merchant_detail');
Route::post('merchants/filter', 'PagesController@filter_merchants');
Route::get('our-company', 'PagesController@company');
Route::get('our-services', 'PagesController@services');
Route::get('news', 'PagesController@news');
Route::get('news/{slug}', 'PagesController@detail_news');
Route::get('contact', 'PagesController@contact');

Route::get('register', 'AccountController@register');
Route::post('register', 'AccountController@sub_register');

Route::get('verification', 'AccountController@verification')->name('vc_page');
Route::post('verification', 'AccountController@verif_account');

Route::get('verif-code', 'AccountController@verif_register');
Route::post('verif-code', 'AccountController@verif_reg_account');

Route::post('sent-verif', 'AccountController@sent_verif');

Route::get('merchants/promo/{slug}', 'PagesController@promo');
Route::get('our-clients', 'PagesController@our_clients');

Route::get('apply', 'PagesController@apply');
Route::post('apply', 'PagesController@apply_sub');

Route::post('add-card', 'AccountController@add_card');
Route::get('parts/cards', 'AccountController@get_card');

Route::group(['prefix' => 'v1'], function(){
	Route::post('get-district', 'AccountController@get_district');
	Route::post('get-subdistrict', 'AccountController@get_subdistrict');
	Route::post('get-postcode', 'AccountController@get_postcode');
});



// Route::get('login', 'AccountController@login');
// Route::post('reset', 'ForgotController@reset');
// Route::get('forgot/{token}', 'ForgotController@index');
// Route::group(['prefix' => 'parts'], function(){
// 	Route::get('mini-cart', 'CheckoutController@miniCart');
// 	Route::get('cart', 'CheckoutController@cartParts');
// });

// Route::group(['prefix' => 'v1'], function(){
// 	Route::post('add-cart', 'ProductController@addCart');
// 	Route::post('remove-cart', 'ProductController@removeCart');
// 	Route::get('total-cart', 'ProductController@totalCart');
// });