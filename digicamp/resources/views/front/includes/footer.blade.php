<div class="row foot_cons">
  <div class="row cus_con">
    <div class="row cus_con2">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="{{url('/')}}"><img src="{{asset('components/both/images/web')}}/Relationship Solution.png" alt="{{$web_name}}" class="img-footer" /></a>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="{{url('/')}}"><img src="{{asset('components/both/images/web')}}/Customer Care.png" alt="{{$web_name}}" class="img-footer" /></a>
              </div>
            </div>
          </div>

          <div class="col-lg-offset-3 col-lg-3 col-md-offset-2 col-md-4 col-sm-offset-1 col-sm-5 col-xs-12">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 foot_link_con">
                @foreach($social_media as $sm)
                    <a href="{{url($sm->value)}}" class="foot_link">
                        <img src="{{asset('components/both/images/web')}}/{{$sm->icon}}" class="foot_soc"></img>
                    </a>
                @endforeach
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <p class="foot_copy">Copyright {{date('Y')}}, Wahana</p>
                  </div>
                </div>
              </div>
            </div>
          </div>          
        </div>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p class="dev_foot">Develop By <a href="{{url('/')}}">Solveway</a></p>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="{{asset('components/plugins/jquery.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/idangerous.swiper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/swiper-slider/swiper.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/global.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.mousewheel.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.jscrollpane.min.js')}}"></script>
<script src="{{asset('components/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/kyubi-head.js')}}"></script>

@stack('custom_scripts')

<!-- Start of Tawk.to Script -->
<script type="text/javascript">
// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='https://embed.tawk.to/5809d4de1e35c727dc0b1ca5/default';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59a53f7b4fe3a1168eada392/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-105582037-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- End of Tawk.to Script -->
