@extends($view_path.'.layouts.master')
@section('content')
<div class="content-push">
	    <!-- Slider main container -->
	<div class="row">
		<div class="swiper-container">
		    <!-- Additional required wrapper -->
		    <div class="swiper-wrapper">
		        <!-- Slides -->
		        @foreach($slide_show as $ss)
		           @if($ss->image)
		        	<div class="swiper-slide hm_slider">
		        		<div class="label-top-left2 hm_1 hm_slider">{!! substr($ss->description, 0, 50) !!}</div>
		        		<img src="{{asset('components/admin/image/slideshow/'.$ss->image)}}" width="100%"/>
		        	</div>
		           @endif
		        @endforeach
		    </div>
		    <!-- If we need pagination -->
		    <div class="swiper-pagination swiper-pagination-index hm_pagination"></div>
		    
		    <!-- If we need navigation buttons -->
		    <div class="swiper-button-prev hm-btn-prev"></div>
		    <div class="swiper-button-next hm-btn-next"></div>
		    
		    <!-- If we need scrollbar -->
		    <div class="swiper-scrollbar"></div>
		    <div class="label-bottom-right btn-slider"><a href="{{url('/register')}}"><button class="button btn-klik-daftar btn-hms">KLIK DISINI UNTUK DAFTAR</button></a></div>
		</div>
	</div>

	<div class="row hm_2 flex_table">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hm_2_1">
			<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 why-us">
				<div class="why-us-title">{{$alasan_1_name->value}}</div>
				<div class="why-us-description">{{$alasan_1->value}}</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hm_2_1">
			<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 why-us">
				<div class="why-us-title">{{$alasan_2_name->value}}</div>
				<div class="why-us-description">{{$alasan_2->value}}</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hm_2_1">
			<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 why-us">
				<div class="why-us-title">{{$alasan_3_name->value}}</div>
				<div class="why-us-description">{{$alasan_3->value}}</div>
			</div>
		</div>
	</div>

	<div class="row hm_3">
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    <div class="row cus_con3">
		    	<div class="header-merchant-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  		<h3>MERCHANTS</h3>
		    	</div>

		    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm_3_1">
		    	  <div class="row">	
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 comments-site-feed padding-sticky">
						@if(isset($headline_merchant_category))
						  <a href="{{url('/merchants-category/'.$headline_merchant_category->category_name)}}">
							<div class="merchant-home-headline" style="background-image:url('{{ asset('components/admin/image/merchant_category/'.$headline_merchant_category->image) }}');">
									<div class="label-center"><span class="hm_3_span">{{strtoupper($headline_merchant_category->category_name)}}</span></div>
							</div>
						  </a>
						@endif
					</div>
					@if(count($headline_merchant_category2) >0)
						@php
							$i = 0;
						@endphp

						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
						  <div class="row">
						  @foreach($headline_merchant_category2 as $sm)
							@php 
							  $i++;
							@endphp 

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-sticky col">
							   <a href="{{ url('/merchants-category/'.$sm->category_name) }}">
								  <div class="merchant-home-list" style="background-image:url('{{ asset('components/admin/image/merchant_category/'.$sm->image) }}');">

								   <div class="label-bottom-right">
								      <span class="hm_4_span">{{ $sm->category_name }}</span>
								   </div>
								  </div>
								</a>
							</div>	
						  @endforeach

						  @if(count($headline_merchant_category2) < 6)
							@php 
								$sisa = 6-count($headline_merchant_category2);
							@endphp

							@for($j = 0; $j<$sisa; $j++)
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-sticky col nhm_con"><div class="merchant-home-list"></div></div>
							@endfor
						  @endif
						</div>
					@endif
					</div>
				  </div>
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm_4">
				  <div class="row flex_table">
					@if(count($sticky_merchant) > 0)
						@foreach($sticky_merchant as $m)
							<div class="col-sm-4 col-xs-12 hm_4_1">
								<a href="{{ url('/merchants/'.$m->slug) }}"><img src="{{ asset('components/admin/image/merchant/'.$m->logo) }}" onerror="this.src='{{ asset('components/admin/image/not_found/merchant-none.png') }}';" class="img-responsive img_center" /></a>
								<div class="merchant-detail">
									<div class="merchant-title">
									  <a href="{{ url('/merchants/'.$m->slug) }}">{{ $m->merchant_name }}</a>
									</div>

									@php
										$gt_city = array(
							       	  				'KAB.', 'KOTA', 'ADM.'
							       	  			   ); 

									 	$city = str_replace($gt_city, ' ', $m->city_name);
									@endphp
									<div class="merchant-city">{{ ucfirst(strtolower(trim($city, ' '))) }}</div>
								</div>
							</div>

							@if(count($sticky_merchant) < 3)
								@php
									$sisa = 3-count($sticky_merchant);
								@endphp

								@for($j = 0; $j<$sisa; $j++)
									<a href="{{ url('/merchants/'.$m->id) }}"></a><img src="{{ asset('components/both/images/web/none.png') }}" width="100%" height="100%" class="img-responsive" />
								@endfor
							@endif
						@endforeach
					@endif
				  </div>
		    	</div>

		    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				  <div class="row ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button class="button btn-default btn-view-all form-control" data-id=""><a href="{{url('/merchants')}}">View All</a></button>
					</div>
				  </div>
				</div>
		    </div>
		</div>
	</div>

	<div class="row hm_5">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  <div class="row cus_con3">
			<div class="header-merchant-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h3>News</h3>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  <div class="row flex_table">
				<div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 comments-site-feed padding-sticky">
					<div class="news-home-headline" style="background-image:url('{{ asset('components/admin/image/news/'.$headline_news->image) }}');">
						<div class="label-bottom-left2"><span class="hm_5_span">{{$headline_news->title}}</span></div>
					</div>

					<div class="news-desc">
						@php
							$content = preg_replace("/<img[^>]+\>/i", "", $headline_news->description);
						@endphp
						<span class="hm_6_span">{!! substr($content, 0, 100) !!}<a href="{{ url('/news') }}/{{ $headline_news->slug }}"> more >></a>
						</span>
					</div>
				</div>

				@if(count($news) > 0)
					@php
						$i = 0;
					@endphp

					<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
					  <div class="row">
						@foreach($news as $sm)
							@php
							$i++;

							$content = preg_replace("/<img[^>]+\>/i", "", $sm->description);
							@endphp

								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-sticky col">
									<div class="news-home-list" style="background-image:url('{{ asset('components/admin/image/news/'.$sm->image) }}'), url('{{ asset('components/admin/image/not_found/none-background.jpg') }}');">
									</div>

									<div class="news-desc">
										<span class="hm_7_span">{!! substr($content, 0, 80) !!}<a href="{{ url('/news') }}/{{ $sm->slug }}"> more >></a>
										</span>
									</div>
								</div>
						@endforeach

						@if(count($news) < 4)
							@php
								$sisa = 6-count($news);
							@endphp

							@for($j = 0; $j<$sisa; $j++)
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-sticky col">
									<div class="news-home-list">
										<a href="{{ url('/news/'.$sm->id) }}"></a>
									</div>
								</div>
							@endfor
						@endif
					  </div>
					</div>
				@endif
			  </div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm_6">
			    <div class="row flex_table">
				@if(count($sticky_news) > 0)
					@foreach($sticky_news as $m)
						@php
							$content = preg_replace("/<img[^>]+\>/i", "", $m->description);
						@endphp

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hm_6_1">
							<a href="{{ url('/news/'.$m->slug) }}"><img src="{{ asset('components/admin/image/news/'.$m->image) }}" onerror="this.src='{{ asset('components/admin/image/not_found/news-none.png') }}';" class="img-responsive img_center" /></a>
							<div class="merchant-detail">
								<div class="merchant-title">{{ $m->title }}</div>
								<div class="merchant-city">{!! $content !!} <a href="{{ url('/news') }}/{{ $m->slug }}" class="hms_news"> more >></a></div>
							</div>
						</div>

						@if(count($sticky_merchant) < 3)
							@php
								$sisa = 3-count($sticky_merchant);
							@endphp

							@for($j = 0; $j<$sisa; $j++)
								<!-- <a href="{{ url('/merchants/'.$m->slug) }}"></a><img src="{{ asset('components/both/images/web/none.png') }}" class="img-responsive img_center" /> -->
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hm_6_1">
									<a href="{{ url('/news/'.$m->slug) }}"></a>
									<div class="merchant-detail">
										<div class="merchant-title"></div>
										<div class="merchant-city"></div>
									</div>
								</div>
							@endfor
						@endif
					@endforeach
				@endif
				</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  <div class="row ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<button class="button btn-default btn-view-all form-control" data-id=""><a href="{{url('/news')}}">View All</a></button>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
	var swiper = new Swiper('.swiper-container', {
	    pagination: '.swiper-pagination',
	    paginationClickable: true,
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	});
</script>
@endpush
