@extends($view_path.'.layouts.master')
@section('content')
@if(session()->has('th_message'))
<input type="hidden" id="th_modal" />

<div class="modal fade" id="thModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content verf_mo_con">
      <div class="modal-header verf_mo_head">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Notification</h4>
      </div>

      <div class="modal-body verf_mo_bod">
        <p>Selamat bergabung, akun Anda sudah diverifikasi. nikmati berbagai manfaat dari CardPlus.</p>
      </div>
    </div>
  </div>
</div>
@endif

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pf_bg"></div>
</div>

<div class="row">
  <div class="container">
    <div class="row cus_con pf_con">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="row">
          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 single_image-photo">
            @if($profile->photo != NULL)
              <img src="{{ asset('components/admin/image/customer') }}/{{ $profile->photo }}" class="img-responsive img_center pf_pc" />
            @else
              <img src="{{ asset('components/admin/image/default3.jpg') }}" class="img-responsive img_center pf_pc" />
            @endif
          </div>
        </div>
      </div>

      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pf_con2">
            <div class="row flex_table">
              @if(count($card) > 0)
                @foreach($card as $crd)
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 pfcon2_1">
                    <img src="{{ asset('components/admin/image/client_card') }}/{{ $crd['image'] }}" class="img-responsive img_center" />
                  </div>
                @endforeach
              @endif

              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pfcon2_1">
                <a href="{{ url('/apply') }}" class="pf_a">
                  <img src="{{ asset('components/front/images/other/plus-icon.jpg') }}" class="img-responsive img_center" />
                </a>
              </div> 
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pf_con3">
            <h3>{{ ucfirst($profile->name) }}</h3>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Tempat tanggal lahir <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    @php
                      $get_ttl = $profile->ttl;
                      if($get_ttl != NULL){
                        $split = explode(",",$get_ttl);
                        $tl = $split[0].', ';
                        $tgl = date_create($split[1]);
                        $get_tgl = date_format($tgl,"d-m-Y");
                      }else{
                        $tl = "";
                        $get_tgl = "";
                      }
                      
                    @endphp

                    @if($tl != NULL || $get_tgl != NULL)
                      <p>{{ $tl.' '.$get_tgl }}</p>
                    @else
                      <p>-</p>
                    @endif
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Jenis Kelamin <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->jenis_kelamin ? $profile->jenis_kelamin : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Status Menikah <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->status_menikah ? $profile->status_menikah : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Alamat <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->address ? $profile->address : '-' }}</p><br>
                    <p>RT : {{ $profile->rt ? $profile->rt : '-' }} / RW : {{ $profile->rw ? $profile->rw : '-' }}</p><br>
                    <p>Kec : {{ $kecamatan != NULL ? $kecamatan->name : '-' }} / Kel : {{ $kelurahan != NULL ? $kelurahan->name : '-' }}</p><br>
                    @if($kota != NULL)
                      <p>{{ucfirst(strtolower($kota)) }}</p><br>
                    @endif

                    @if($kodepos != NULL)
                      <p>{{ $kodepos->postcode }}</p>
                    @endif
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Email <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->email ? $profile->email : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Nomor Telepon <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->telephone ? $profile->telephone : '-' }}</p>
                  </div>
                </div>
              </div>


              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Nomor Handphone <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->handphone ? $profile->handphone : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Pekerjaan <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->pekerjaan ? $profile->pekerjaan : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Agama <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->agama ? $profile->agama : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Pendidikan <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->pendidikan ? $profile->pendidikan : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Tujuan Pakai <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->tujuan ? $profile->tujuan : '-' }}</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Pengeluaran Perbulan <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->pengeluaran ? $profile->pengeluaran : '-' }}</p>
                  </div>
                </div>
              </div>

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                    <p>Hobby <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                    <p>{{ $profile->hobby ? $profile->hobby : '-' }}</p>
                  </div>
                </div>
              </div>

              @if(count($card) > 0)
                @foreach($card as $q => $crd)
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row pf_p">
                    <div class="col-lg-5 col-md-6 col-sm-10 col-xs-12 pf_ip_1">
                      <p>Nomor Kartu {{ $q+1 }}<span class="tw_dot">:</span></p>
                    </div>

                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pf_ip_2">
                      <p>{{ $crd['no_kartu'] }}</p>
                    </div>
                  </div>
                </div>
                @endforeach
              @else
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row pf_p">
                  <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 pf_ip_1">
                    <p>Nomor Kartu <span class="tw_dot">:</span></p>
                  </div>

                  <div class="col-lg-5 col-md-6 col-sm-4 col-xs-12 pf_ip_2">
                    <p>-</p>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pf_sub">
            <div class="row">
              <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 pf_sub1">
                <a href="{{ url('/edit-profile') }}"><button class="btn btn-default">Edit Profile</button></a>
              </div>

              <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 pf_sub2">
                <a href="{{ url('/change-password') }}"><button class="btn btn-default">Ubah Password</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('custom_scripts')
<script>
$(document).ready(function(){
    if($('#th_modal').length > 0){
      $("#thModal").modal("show");
    }
  });
</script>
@endpush
