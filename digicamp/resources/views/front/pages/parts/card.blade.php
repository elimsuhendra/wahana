<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3 class="edp_text pf_card">Data Kartu</h3>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
	    <table class="table table-striped">
	    	<thead>
	        <tr>
	            <th>Klien</th>
	            <th>Nomor Kartu</th>
	            <th>Tipe</th>
	        </tr>
	        </thead>

	        @foreach($card as $crd)
	        <tr>
	            <td>{{ $crd['client'] }}</td>
	            <td>{{ $crd['no_kartu'] }}</td>
	            <td>{{ $crd['type'] }}</td>
	        </tr>
	        @endforeach
	    </table>
	</div>
</div>