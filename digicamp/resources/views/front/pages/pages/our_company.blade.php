@extends($view_path.'.layouts.master')
@section('content')
<div class="row cus_con oc_con">
  <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
  	<div class="row">
  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  <ul class="nav nav-pills nav-justified cus_tab_oc">
		  	@foreach($content as $q => $oc_con)
		   	  <li class="oc_mbl oc-menu">
		   	    <a href="#menu_{{ $q }}" data-toggle="pill">
		   	      <img id="oc-menu_{{ $q }}" data-hv="{{ asset('components/admin/image/our_company') }}/{{ $oc_con->image }}" src="{{ asset('components/admin/image/our_company/'.$oc_con->image ) }}" class="img-responsive img_center ac-img" style="{{$q == 0 ? 'display: none' : 'display: inline'}}"/>

		   	      <img class="oc-menu-active" id="oc-menu-active_{{ $q }}" data-hv="{{ asset('components/admin/image/our_company') }}/{{ $oc_con->image_hover }}" src="{{ asset('components/admin/image/our_company/'.$oc_con->image_hover) }}" class="img-responsive img_center" style="{{$q == 0 ? 'display: inline' : 'display: none'}}"/>
		   	       <p class="oc_tab_p">{{ $oc_con->our_company_name }}</p>
		   	    </a>
		   	  </li>
		  	@endforeach
		  </ul>
	  </div>
	</div>

	<div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tab-content">
	    @foreach($content as $q => $oc_con)
		  <div id="menu_{{ $q }}" class="tab-pane fade {{ $loop->first ? 'in active' : '' }} oc_tab_des">
			{!! $oc_con->description !!}
		  </div>	
		@endforeach
	  </div>
	</div>
  </div>
</div>
@endsection

@push('custom_scripts')
<script>
	$(document).ready(function(){
		$('.oc-menu').click(function(){
			var id = $( ".ac-img", this).attr('id');
			id = id.split('_');

			$(id).css("display", "none");
			$('.oc-menu-active').css("display", "none");
			$('.ac-img').css("display", "inline");
			$('#oc-menu_'+id[1]).css("display", "none");
			$('#oc-menu-active_'+id[1]).css("display", "inline");
		});
	});
</script>
@endpush