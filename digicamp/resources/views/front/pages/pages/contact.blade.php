@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12 ct_banner" style="background-image: url('{{ asset('components/admin/image/banner')  }}/{{ $content->image  }}')">
  	<div class="row">
  	  <div class="container">
  	    <div class="row">
          <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">
      	     {!! $content->description !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row cus_con4">
  	<div class="row cus_con4">
  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ct_con1 flex_table">
      	<div class="col-md-8 col-sm-12 col-xs-12">
     		  <div class="row">
     		  	<div class="col-md-12 col-sm-12 col-xs-12 ct_table">
      	 		  <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 ct_input1">
                      <p>Email <span class="tw_dot">:</span></p>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 ct_input2">
                      <p>{{ $web_email }}</p>
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 ct_input1">
                      <p>Customer Support <span class="tw_dot">:</span></p>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 ct_input2">
                      <p>{{ $cs_email }}</p>
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 ct_input1">
                      <p>Call Us <span class="tw_dot">:</span></p>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 ct_input2">
                      <p>{{ $phone }}</p>
                    </div>
                  </div>
                </div>
              </div>
      	 		</div>

      	 		<div class="col-md-12 col-sm-12 col-xs-12 ct_con2">
      	 		  <h3>Live Chat</h3>
      	 		</div>

      	 		<div class="col-md-12 col-sm-12 col-xs-12 ct_con3">
      	 		  <a href="javascript:void(Tawk_API.popup())"><p>Chat via browser</p></a>
      	 		</div>

      	 		<div class="col-md-12 col-sm-12 col-xs-12 ct_con4">
      	 		  <p>Serving you 24 hours every day, non-stop</p>
      	 		</div>
    	 	  </div>
      	</div>

      	<!-- <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
      	  <div class="row ct_sos_con">
      	  	<div class="col-md-12 col-sm-12 col-xs-12 ct_sos_1">
      	  	  <h3>SOSIAL MEDIA</h3>
      	  	</div>

      	  	@foreach($medsos as $sos)
      	  	<div class="col-md-12 col-sm-12 col-xs-12 flex_table ct_sos_3a">
      	  	  <a href="{{ $sos->value }}"><img src="{{ asset('components/front/images/other') }}/{{ $sos->name.'.jpg' }}" class="img-responsive ct_sos_img" /></a>
      	  	  <a href="{{ $sos->value }}" class="ct_sos_2a"><p class="ct_sos_2">{{ ucfirst($sos->name) }}</p></a>
      	  	</div>
      	  	@endforeach
      	  </div>
      	</div> -->
  	  </div>
  	</div>
</div>

<div class="row ct_g">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row">
     <iframe src="{{ $google_map }}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

     <div class="ct_gt_con">
      <div class="ct_gt_pad">
     	  <p class="ct_gtl">Operation and Customer Care</p>

        <div class="ct_gda">{!! $address !!}</div>

        <img src="{{ asset('components/back/images/admin') }}/{{ $web_logo }}" class="img-responsive img_center ct_gimg" />
      </div>
     </div>
    </div>
  </div>
</div>
@endsection
