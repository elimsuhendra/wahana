@extends($view_path.'.layouts.master')
@section('content')
<div class="row cus_con os_con">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="row">
	  <div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
	  	<div class="row">
		  <ul class="nav nav-tabs osr_tab flex_table">
		  @foreach($content as $q => $os_con)
		    <li class="{{ $loop->first ? 'active' : '' }} os_cus_tab icon-our-service">
		      <a data-toggle="tab" href="#menu_{{ $q }}" class="os_mnu">
		      	<img id="icon-our-service_{{$q}}" src="{{ asset('components/admin/image/our_service') }}/{{ $os_con->icon }}" class="img_center img-responsive icn-img" style="display:{{ $q == 0 ? 'none' : 'block'}};margin:auto;"/>
		      	<img id="icon-our-service-active_{{$q}}" src="{{ asset('components/admin/image/our_service') }}/{{ $os_con->image_active }}" class="img_center img-responsive icon-our-service-active" style="display:{{$q == 0 ? 'block' : 'none'}}; margin:auto;" />
		      
		      	<p class="os_tab_p">{!! str_replace(" ", "<br>", $os_con->our_service_name) !!}</p>
		      </a>
		    </li>
		  @endforeach
		  </ul>
		</div>
	  </div>
	</div>

	<div class="row os_tab_content">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	  	<div class="tab-content">
	  	  @foreach($content as $q => $os_con)
		  <div id="menu_{{ $q }}" class="tab-pane fade {{ $loop->first ? 'in active' : '' }}">
		  	<div class="row">
		  		{!! $os_con->description !!}
		  	</div>
		  </div>
		  @endforeach
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@push('custom_scripts')
<script>
	$(document).ready(function(){
		$('.icon-our-service').click(function(){
			var id = $( ".icn-img", this).attr('id');
			id = id.split('_');
			
			$(id).css("display", "none");
			$('.icon-our-service-active').css("display", "none");
			$('.icn-img').css("display", "block");
			$('#icon-our-service_'+id[1]).css("display", "none");
			$('#icon-our-service-active_'+id[1]).css("display", "block");
		});
	});
</script>
@endpush