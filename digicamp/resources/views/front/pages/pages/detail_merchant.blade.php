@extends($view_path.'.layouts.master')
@section('content')
<div class="row cus_con mc_con">
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 dm_tbs">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 dm_tl">
        <h3>{{ strtoupper($client->merchant_name) }}</h3>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <img src="{{ asset('components/admin/image/merchant') }}/{{ $client->headline_image }}" onerror="this.src='{{ asset('components/admin/image/not_found/detail-merchant-none.jpg') }}';" class="img-responsive img_center img_width" />
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <ul class="nav nav-pills nav-justified dm_tab">
            <li class="active"><a data-toggle="pill" href="#menu_1">CONTACT US</a></li>
            <li><a  data-toggle="pill" href="#menu_2">OUTLET</a></li>
            <li><a  data-toggle="pill" href="#menu_3">PROMO</a></li>
          </ul>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12 dm_tab_con">
        <div class="tab-content">
          <div id="menu_1" class="tab-pane dm_tab_pad fade in active">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 dm_tab_tl1">
                <h3>CONTACT US</h3>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 dm_tab_content1">
                <div class="row">
                  <div class="col-md-9 col-sm-9 col-xs-12 dm_table">
                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 dm_input1">
                        <p>Phone <span class="tw_dot">:</span></p>
                      </div>

                      <div class="col-lg-4 col-md-8 col-sm-7 col-xs-12 dm_input2">
                        <p>{{ $client->phone }}</p>
                      </div>
                    </div>

                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 dm_input1">
                        <p>Address <span class="tw_dot">:</span></p>
                      </div>

                      <div class="col-lg-4 col-md-8 col-sm-7 col-xs-12 dm_input2">
                        <p>{{ $client->address }}</p>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 dm_input1">
                        <p>Email <span class="tw_dot">:</span></p>
                      </div>

                      <div class="col-lg-4 col-md-8 col-sm-7 col-xs-12 dm_input2">
                        <p>{{ $client->email }}</p>
                      </div>
                    </div>
                  </div>

                  <div class="row dm_img">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <img src="{{ asset('components/admin/image/merchant') }}/{{ $client->logo }}" onerror="this.src='{{ asset('components/admin/image/not_found/merchant-none.png') }}';" class="img-responsive img_center" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="menu_2" class="tab-pane dm_tab_pad fade">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 dm_tab_content2">
              @if(count($outlet) > 0)
                <div class="row">
                  <div class="col-lg-3 col-md-4 col-sm-8 col-xs-12 dm_tb_con1">
                    <h3>Outlet <span class="tw_dot">:</span></h3>
                  </div>

                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="row">
                      @foreach($outlet as $q => $ot)
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <p><span>{{ $q+1 }}</span> {{ $ot->outlet_name }}</p>
                        </div>
                      @endforeach
                      </div>
                  </div>
                </div>
              @else
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <h3>No Outlet Available</h3>
                </div>
              @endif
              </div>
            </div>
          </div>

          <div id="menu_3" class="tab-pane dm_tab_pad2 fade">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 dm_tab_content3">
                <h3>Promo Bulan Ini :</h3>
              </div>
              @if(count($promo) > 0)
                @foreach($promo as $prm)
                <div class="col-md-12 col-sm-12 col-xs-12 dm_promo">
                  <a href="{{ url('/merchants/promo') }}/{{ $prm->promo_slug }}">
                    <img src="{{ asset('components/admin/image/promo') }}/{{ $prm->image }}" onerror="this.src='{{ asset('components/admin/image/not_found/promo-none.png') }}';" class="img-responsive img_center img_width" />
                  </a>
                </div>
                @endforeach
              @else
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <h3>No Promo Available</h3>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="row">
      <div class="col-lg-offset-2 col-lg-10 col-md-offset-2 col-md-10 col-sm-offset-2 col-sm-10 col-xs-12">
        @if(count($populer) > 0)
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mc_mp_tl">
            <h3>MOST POPULAR</h3>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row mc_mp_logo">
            @foreach($populer as $pop)
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a href="{{ url('/merchants') }}/{{ $pop->slug }}">
                  <img src="{{ asset('components/admin/image/merchant') }}/{{ $pop->logo }}" onerror="this.src='{{ asset('components/admin/image/not_found/merchant-none.png') }}';" class="img-responsive img_center" />  
                </a>              
              </div>
            @endforeach
            </div>
          </div>
        </div>
        @endif

        @if(count($related) > 0)
        <div class="row dm_rel">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mc_mp_tl">
            <h3>YOU MIGHT LIKE</h3>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row mc_mp_logo">
            @foreach($related as $rel)
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a href="{{ url('/merchants') }}/{{ $rel->slug }}">
                  <img src="{{ asset('components/admin/image/merchant') }}/{{ $rel->logo }}" onerror="this.src='{{ asset('components/admin/image/not_found/merchant-none.png') }}';" class="img-responsive img_center" />  
                </a>              
              </div>
            @endforeach
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection

