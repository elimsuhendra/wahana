@extends($view_path.'.layouts.master')
@section('content')
<div class="content-push">
   <div class="row">
    <!-- Slider main container -->
	<div class="swiper-container or_cl" id="grad">
	    <!-- Additional required wrapper -->
	    <div class="swiper-wrapper">
	        <!-- Slides -->
	        <input type="hidden" id="src-url" value="{{ url('/') }}">
	        <input type="hidden" id="src-img" value="{{asset('components/admin/image/client_card/')}}">
	        <input type="hidden" id="client_card" value="{{$client_card}}">
	        @foreach($client as $c)
	     		<div class="swiper-slide or_cl_slide" id="slide_{{$c->id}}">
	     			<img src="{{asset('components/admin/image/client/'.$c['logo'])}}" onerror="this.src='{{ asset('components/admin/image/not_found/client-none.png') }}';" class="img-responsive" />
	     		</div>
            @endforeach
	    </div>
	    <!-- If we need pagination -->
	    <!-- <div class="swiper-pagination swiper-pagination-index" style="bottom: 40px !important; text-align: center;left: 0px !important;"></div> -->
	    
	    <!-- If we need navigation buttons -->
	    <div class="arrow-swipper-prev swiper-button-prev oc-btn-prev"></div>
	    <div class="arrow-swipper-next swiper-button-next oc-btn-next"></div>
	    
	    <!-- If we need scrollbar -->
	    <div class="swiper-scrollbar"></div>
	</div>
  </div>
</div>

<div class="row or_cl_con">
  <div class="container">
  	<div class="row cus_con">
  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  	    <div class="row">
		  	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  	  <img class="img-card-header img-responsive img_center" src="" width="" />	 
		  	</div>

		  	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 card-header-right">
		  	  <div class="card-title"></div>
			  <div class="card-desc"></div>
		  	</div>
	  	</div>
  	  </div>
  	</div>
  </div>
</div>

<div class="row or_cl_con2">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row cus_con">
	  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 or_cl_2">
	  	  	<div class="row card-collections flex_table">
	  	  	</div>

	  	  	<div class="row">
		  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 oc_btn_apl">
			  	<button class="button oc_btn_ap" data-id=""><a href="{{url('/apply')}}">APPLY NOW</button>
			  </div>
		  	</div>
	  	</div>
  	</div>
  </div>
</div>
@endsection

@push('custom_scripts')
<script>
	$(document).ready(function(){
		var swiper = new Swiper('.swiper-container', {
		        pagination: '.swiper-pagination',
		        slidesPerView: 3,
		        centeredSlides: true,
		        paginationClickable: true,
		        spaceBetween: 30,
		        grabCursor: true,
		        nextButton: '.swiper-button-next',
			    prevButton: '.swiper-button-prev',
			    onSlideChangeStart: function(){
			    	changeContent();
			    },
		    });

		function changeContent(){
			var url = $('#src-url').val();
			var slide_active = $('.swiper-slide-active').attr('id');

			var id = slide_active.split('_');
			id = id[1];
			var src_img = $('#src-img').val();
			var client_card = JSON.parse($('#client_card').val());

			$(".img-card-header").attr("src", '');
			$(".card-title").text('');
			$(".card-desc").text('');
			for(var i=0; i<client_card.length; i++){
				if(client_card[i]['id_client'] == id){
					if(client_card[i]['image'] != ""){
					   $(".img-card-header").attr("src", src_img + '/' +client_card[i]['image']);

					   $(".img-card-header").error(function () {
					   	$(this).attr("src", ''+ url +'/components/admin/image/not_found/client-none.png');
					   });
					} else{
					   $(".img-card-header").attr("src", url + 'components/admin/image/not_found/client-none.png');
					}
					$(".card-title").text(client_card[i]['client_card_name']);
					$(".card-desc").text(client_card[i]['description']);
					break;
				}
			}
			var k = 1;
		
			var text = '';
			for(var j=0; j<client_card.length; j++){
				if(client_card[j]['id_client'] == id){
					if(client_card[j]['image'] != ""){
						text += 	'<div class="col-lg-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 or_cl_3">'+
						  	  	  	'<img class="card-img img-responsive" id="card-img_1" src="'+src_img + '/' +client_card[j]['image']+'" />'+
									'<div class="card-title2" id="card-title2_1">'+ client_card[j]['client_card_name'] +'</div>'+
					  	  	  	'</div>';
		  	  	  } else{
		  	  	  		text += 	'<div class="col-lg-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 or_cl_3">'+
						  	  	  	'<img class="card-img img-responsive" id="card-img_1" src="'+url + 'components/admin/image/not_found/client-none.png" />'+
									'<div class="card-title2" id="card-title2_1">'+ client_card[j]['client_card_name'] +'</div>'+
					  	  	  	'</div>';
		  	  	  }
				}
					
			}
			$('.card-collections').text('');
			$('.card-collections').append(text);

			$(".or_cl_3 img").error(function () {
			  $(this).attr("src", ""+ url +"/components/admin/image/not_found/client-none.png");
			});
		}

		changeContent();
	});

</script>
@endpush
