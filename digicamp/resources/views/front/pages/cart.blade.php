@extends($view_path.'.layouts.master')
@section('content')
<div class="content-push">
	<div class="breadcrumb-box">
        <a href="{{url('/')}}">Home</a>
        <a href="{{url('cart')}}">Cart</a>
    </div>
    <div class="information-blocks">
    	<div class="loader-container cart-loader">
            <div class="cart-loader"></div>
            <div class="cart-loader"></div>
            <div class="cart-loader"></div>
        </div>
	    <div class="cart-parts"></div>
    </div>
</div>
@endsection