@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$client->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">

       
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'client_name','label' => 'Name','value' => (old('client_name') ? old('client_name') : $client->client_name),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

        <div class="form-group form-md-line-input col-md-12">
            <label>Logo</label><br>
            <label class="btn green input-file-label-logo">
              <input type="file" class="form-control col-md-12 single-image" name="logo"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="logo">Hapus</button>
              <input type="hidden" name="remove-single-image-logo" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: 334 x 273 px</small>

            <div class="form-group single-image-logo col-md-12">
              <img src="{{asset($image_path.'/'.$client->logo)}}" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div>
      </div>
       <hr/>
      <div class="row client-data">
            <div class="form-group col-md-12 form-group-promo">
              <label for="tag" class="sub-title">Client Card</label>
            </div>
            <input type="hidden" id="no_client_card" value="{{(count($data3) > 0 ? count($data3) + 1: 1)}}">

            <div class="form-group col-md-12 header-dropdown" id="header-dropdown_0">
             <!--  <div class="sub-title-header" style="">Promo:</div> -->
              <button class="btn dropdown-toggle red-wahana show-client-card" id="show-client-card_0" type="button" data-toggle="dropdown" aria-expanded="false">  Client: 0 
              <span class="caret"></span></button>
              <button type="button" class="btn btn-danger delete-outlet red-wahana add-client-card" id="add-client-card_0"><i class="fa fa-plus"></i></button>
            </div>

            <div class="form-group col-md-12 content-dropdown" id="content-dropdown_0" style="display: none;">
               <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control client-card-name" id="client-card-name_0" name="client_card_name[]" value=""  placeholder="Client Card Name">
                    <label for="form_floating_hg1">Card Name <span class="" aria-required="true"></span></label>
                    <small></small>
                  </div>
                </div>


                <div class="form-group col-md-6">
                  <label for="tag">Card Category</label>
                  <input type="hidden" name="">
                  <select name="client_card_card_category[]" class="form-control client-card-card-category" id="client-card-card-category_0">
                    @foreach($data1 as $d1)
                      <option value="{{$d1->id}}">{{$d1->card_category_name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-md-12">
                  <div class="form-group form-md-line-input">
                    <textarea class="form-control client-card-description" id="client_card-description_0" name="client_card_description[]"  placeholder="Client Card Description"></textarea>
                    <label for="form_floating_ZV5">Card Description <span class="" aria-required="true"></span></label>
                  </div>
                </div>

                <!-- <div class="col-md-12">
                  <div class="form-group form-md-line-input">
                    <input class="form-control client_card-no-card" id="client-card-no-card_0" name="client_card_no_card[]"  placeholder="No Card"></input>
                    <label for="form_floating_J4T">No Card<span class="" aria-required="true"></span></label>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group form-md-line-input">
                    <input class="form-control client-card-code-card" id="client-card-code-card_0" name="client_card_code_card[]"  placeholder="Code Card"></input>
                    <label for="form_floating_J4T">Code Card<span class="" aria-required="true"></span></label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                    <input type="text" id="client-card-end-date_0" class="form-control datepicker client-card-end-date" name="client_card_end_date[]" value="" readonly="" placeholder="Valid Thru">
                    <label for="form_floating_Hqd">Valid Thru <span class="" aria-required="true">*</span></label>
                    <small></small>
                  </div>
                </div> -->

                <div class="form-group form-md-line-input col-md-12">
                    <label>Image</label><br>
                    <label class="btn green input-file-label-client-card-image">
                      <input type="hidden" name="client_card_image_index[]" value="0" >
                      <input type="file" class="form-control col-md-12 single-image client-card-image" name="client_card_image_0"> Pilih File
                    </label>
                      <br>
                    <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: 382 x 243 px</small>
                    <input type="hidden" value="" name="img_client_card_image[]">
                    <div class="form-group single-image-client_card_image_0 col-md-12">
                      <img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail img-client-card-image-0">
                    </div>
                </div>
            </div>
            

            @if(count($data3) > 0)
                @php
                      $i = 0;
                @endphp
                @foreach($data3 as $p)
                    @php
                      $i++;
                    @endphp
                    
                    <div class="form-group col-md-12 header-dropdown" id="header-dropdown_{{$i}}">
                      <!--  <div class="sub-title-header" style="">Promo:</div> -->
                      <button class="btn dropdown-toggle red-wahana show-client-card" id="show-client-card_{{$i}}" type="button" data-toggle="dropdown" aria-expanded="false">  Client: {{$p->client_card_name}} 
                      <span class="caret"></span></button>
                    </div>

                  <div class="form-group col-md-12 content-dropdown" id="content-dropdown_{{$i}}" style="display: none;">
                     <div class="col-md-6">
                        <div class="form-group form-md-line-input">
                          <input type="text" class="form-control client-card-name" id="client-card-name_{{$i}}" name="client_card_name[]" value="{{$p->client_card_name}}"  placeholder="Client Card Name">
                          <label for="form_floating_hg1">Card Name <span class="" aria-required="true"></span></label>
                          <small></small>
                        </div>
                      </div>

                      <div class="form-group col-md-6">
                        <label for="tag">Card Category</label>
                        <select name="client_card_card_category[]" class="form-control client-card-card-category" id="client-card-card-category_{{$i}}" value="{{$p->id_card_category}}">
                          @foreach($data1 as $d1)
                            <option value="{{$d1->id}}" {{$p->id_card_category == $d1->id ? 'selected' : ''}}>{{$d1->card_category_name}}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-12">
                        <div class="form-group form-md-line-input">
                          <textarea class="form-control client-card-description" id="client_card-description_{{$i}}" name="client_card_description[]"  placeholder="Client Card Description">{{$p->description}}</textarea>
                          <label for="form_floating_ZV5">Card Description <span class="" aria-required="true"></span></label>
                        </div>
                      </div>

                      <!-- <div class="col-md-12">
                        <div class="form-group form-md-line-input">
                          <input class="form-control client_card-no-card" id="client-card-no-card_{{$i}}" name="client_card_no_card[]"  placeholder="No Card" value="{{$p->no_card}}"></input>
                          <label for="form_floating_J4T">No Card<span class="" aria-required="true"></span></label>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <div class="form-group form-md-line-input">
                          <input class="form-control client-card-code-card" id="client-card-code-card_{{$i}}" name="client_card_code_card[]"  placeholder="Code Card" value="{{$p->code_card}}"></input>
                          <label for="form_floating_J4T">Code Card<span class="" aria-required="true"></span></label>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group form-md-line-input">
                          <input type="text" id="client-card-end-date_{{$i}}" class="form-control datepicker client-card-end-date" name="client_card_end_date[]" value="{{date_format(date_create($p->end_date),'Y-m-d')}}" readonly="" placeholder="Valid Thru">
                          <label for="form_floating_Hqd">Valid Thru <span class="" aria-required="true">*</span></label>
                          <small></small>
                        </div>
                      </div> -->

                      <div class="form-group form-md-line-input col-md-12">
                          <label>Image</label><br>
                          <label class="btn green input-file-label-client-card-image">
                            <input type="hidden" name="client_card_image_index[]" value="{{$i}}" >
                            <input type="file" class="form-control col-md-12 single-image client-card-image" name="client_card_image_{{$i}}"> Pilih File
                          </label>
                            <br>
                          <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: 382 x 243 px</small>
                          <input type="hidden" name="img_client_card_image[]" value="{{$p->image}}">
                          <div class="form-group single-image-client_card_image_{{$i}} col-md-12">
                            <img src="{{asset($image_path3.'/'.$p->image)}}" class="img-responsive thumbnail single-image-thumbnail img-client-card-image-0">
                          </div>
                      </div>
                  </div>
                @endforeach
              @endif
      </div>
      {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
    //   $('input,select,checkbox,button.remove-single-image').attr('disabled',true);
      $(document).on('click','.add-outlet',function(){
        var outlet_name2       = $('.outlet_name').val();
        var outlet_address2    = $('.outlet_address').val();
        var outlet_phone2      = $('.outlet_phone').val();
        var outlet_email2      = $('.outlet_email').val();

        var error       = '';
        var returnfalse =   0;
        if(outlet_name2 == ""){
            returnfalse = 1;
            error = 'Outlet Name required !';
        }else if(outlet_address2 == ""){
            returnfalse = 1;
            error = 'Outlet Address required !';
        }

        if(returnfalse == 1){
          alert(error);
        }else{
            var temp        = '<tr><td>'+outlet_name2+'</td><td>'+outlet_phone2+'</td><td>'+outlet_email2+'</td><td>'+outlet_address2+'<input type="hidden" name="outlet_name2[]" value="'+outlet_name2+'"><input type="hidden" name="outlet_address2[]" value="'+outlet_address2+'"><input type="hidden" name="outlet_phone2[]" value="'+outlet_phone2+'"><input type="hidden" name="outlet_email2[]" value="'+outlet_email2+'"></td><td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td></tr>';
            $('.outlet-data').append(temp);
        }
        
      });

      $(document).on('click','.add-client-card',function(){ 
          var id = $(this).attr('id');
          id = id.split('_');
          id = id[1];
          var client_card_name              = $('#client-card-name_'+id).val();
          var client_card_card_category     = $('#client-card-card-category_'+id).val();
          var client_card_description       = $('#client-card-description_'+id).val();
          var client_card_no_card           = $('#client-card-no-card'+id).val();
          var client_card_code_card         = $('#client-card-code-card_'+id).val();
          var client_card_end_date          = $('#client-card-end-card_'+id).val();

          var error       = '';
          var returnfalse =   0;
          if(client_card_name == ""){
            error = "Client Card Name required !";
            returnfalse =   1;
          }
          // alert(returnfalse);
          if(returnfalse == 1){
            alert(error);
          }else{
            var i = $("#no_client_card").val();
            // alert('i: '+i);
            var form =  '<div class="form-group col-md-12 header-dropdown" id="header-dropdown_'+i+'">'+
                          '<button class="btn dropdown-toggle red-wahana show-client-card" id="show-client-card_'+i+'" type="button" data-toggle="dropdown" aria-expanded="false">  Client: '+i+ 
                          '<span class="caret"></span></button>'+
                        '</div>'+

                      '<div class="form-group col-md-12 content-dropdown" id="content-dropdown_'+i+'" style="display: none;">'+
                         '<div class="col-md-6">'+
                            '<div class="form-group form-md-line-input">'+
                              '<input type="text" class="form-control client-card-name" id="client-card-name_'+i+'" name="client_card_name[]" value="" placeholder="Client Card Name">'+
                              '<label for="form_floating_hg1">Card Holder Name <span class="" aria-required="true"></span></label>'+
                              '<small></small>'+
                            '</div>'+
                          '</div>'+

                          '<div class="form-group col-md-6">'+
                            '<label for="tag">Card Category</label>'+
                            '<select name="client_card_card_category[]" class="form-control client-card-card-category" id="client-card-card-category_'+i+'" value="">'+
                              '@foreach($data1 as $d1)'+
                                '<option value="{{$d1->id}}">{{$d1->card_category_name}}</option>'+
                              '@endforeach'+
                            '</select>'+
                          '</div>'+

                          /*'<div class="col-md-12">'+
                            '<div class="form-group form-md-line-input">'+
                              '<textarea class="form-control client-card-description" id="client-card-description_'+i+' " name="client_card_description[]"  placeholder="Client Card Description"></textarea>'+
                              '<label for="form_floating_ZV5">Card Description <span class="" aria-required="true"></span></label>'+
                            '</div>'+
                          '</div>'+

                          '<div class="col-md-12">'+
                            '<div class="form-group form-md-line-input">'+
                              '<input class="form-control client_card-no-card" id="client-card-no-card_'+i+' " name="client_card_no_card[]"  placeholder="No Card" value=""></input>'+
                              '<label for="form_floating_J4T">No Card<span class="" aria-required="true"></span></label>'+
                            '</div>'+
                          '</div>'+

                          '<div class="col-md-12">'+
                            '<div class="form-group form-md-line-input">'+
                              '<input class="form-control client-card-code-card" id="client-card-code-card_'+i+'" name="client_card_code_card[]"  placeholder="Code Card" value=""></input>'+
                              '<label for="form_floating_J4T">Code Card<span class="" aria-required="true"></span></label>'+
                            '</div>'+
                          '</div>'+

                          '<div class="col-md-6">'+
                            '<div class="form-group form-md-line-input">'+
                              '<input type="text" id="client-card-end-date_'+i+' " class="form-control datepicker client-card-end-date" name="client_card_end_date[]" value="" readonly="" placeholder="Valid Thru">'+
                              '<label for="form_floating_Hqd">Valid Thru <span class="" aria-required="true">*</span></label>'+
                              '<small></small>'+
                            '</div>'+
                          '</div>'+*/

                          '<div class="form-group form-md-line-input col-md-12">'+
                              '<label>Image</label><br>'+
                              '<label class="btn green input-file-label-client-card-image">'+
                                '<input type="hidden" name="client_card_image_index[]" value="'+i+'" >'+
                                '<input type="file" class="form-control col-md-12 single-image client-card-image" name="client_card_image_'+i+'"> Pilih File'+
                              '</label>'+
                                '<br>'+
                              '<small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>'+
                              '<input type="hidden" name="img_client_card_image[]" value="">'+
                              '<div class="form-group single-image-client_card_image_'+i+' col-md-12">'+
                                '<img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail img-client-card-image-0">'+
                              '</div>'+
                          '</div>'+
                      '</div>';
            $('.client-data').append(form);
            // $.each(selectValues, function(key, value) {   
            //   $('#client-card-card-category_'+i)
            //      .append($("<option></option>")
            //                 .attr("value",key)
            //                 .text(value)); 
            // });
            i++;
            $("#no_client_card").val(i);
            $.datepickers();
          }
      });

       $(document).on('click','.show-client-card',function(){
          var id = $(this).attr('id');
          id = id.split("_");
          id = id[1];
          $('#content-dropdown_'+id).toggle();
          // alert('#content-dropdown_'+id);
       }); 

        $(document).on('click','.delete-promo',function(){
          var id = $(this).attr('id');
          id = id.split("_");
          id = id[1];
          // alert(id);
          $('#header-dropdown_'+id).remove();
          $('#content-dropdown_'+id).remove();
        })

      // $(document).on('click','.add-promo',function(){
      //   var data1      = $('.promo_name').val();
      //   var data2      = $('.promo_descripton').val();
      //   var data3      = $('.term_condition').val();
      //   var src        = $('.img-promo_image').attr("src");
      //   var img        = "<img src='"+src+"' class='img-responsive thumbnail single-image-thumbnail'>";
      //   var file       = $(".promo_image")[0].files;


      //   var temp        = '<tr><td>'+data1+'</td><td>'+data2+'</td><td>'+data3+'</td><td>'+file+'<input type="hidden" name="promo_name2[]" value="'+data1+'"><input type="hidden" name="promo_descripton2[]" value="'+promo_descripton2+'"><input type="hidden" name="term_condition2[]" value="'+term_condition2+'"><input type="hidden" name="term_condition2[]" value="'+term_condition2+'"></td><td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td></tr>';
      //   $('.outlet-data').append(temp);
      // });

      $(document).on('click','.delete-outlet',function(){
        $(this).closest('tr').remove();
      })
    });
  </script>
@endpush
@endsection
