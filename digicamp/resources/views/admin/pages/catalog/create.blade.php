@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green portlet-container">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
        <div class="head-button">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint"><i class="fa fa-arrow-left"></i> {{trans('general.back')}}</button></a>
          {!!view($view_path.'.builder.button',['type' => 'submit','label' => 'Lanjutkan'])!!}
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      {!!view($view_path.'.builder.text',['name' => 'catalog_name','label' => 'Catalog Name','value' => (old('catalog_name') ? old('catalog_name') : ''),'attribute' => 'required'])!!}
      {!!view($view_path.'.builder.textarea',['name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : ''),'attribute' => 'required'])!!}
      <div class="form-group">
        <label for="tag">Store</label>
        <select class="select2" name="store_id[]" multiple="multiple">
          @if(!is_array(json_decode(auth()->guard($guard)->user()->store_id)) && in_array(auth()->guard($guard)->user()->store_id,[0,1]))
            <option value="0" {{old('store_id') ? (in_array(0,old('store_id')) ? 'selected' : '') : ''}}>-- All Store --</option>
          @endif
          @foreach($store as $s)
            <option value="{{$s->id}}" {{old('store_id') ? (in_array($s->store_id,old('store_id')) ? 'selected' : '') : ''}}>{{$s->store_name}}</option>
          @endforeach
        </select>
      </div>
      {!!view($view_path.'.builder.text',['name' => 'end_date','label' => 'Valid Until','value' => (old('end_date') ? old('end_date') : ''),'class' => 'datepicker','attribute' => 'required readonly'])!!}
    </div>
  </div>
</form>
@endsection
