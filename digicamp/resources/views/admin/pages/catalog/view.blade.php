@extends($view_path.'.layouts.master')
@section('content')
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green portlet-container">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
        <div class="head-button">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint"><i class="fa fa-arrow-left"></i> {{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      {!!view($view_path.'.builder.text',['name' => 'catalog_name','label' => 'Catalog Name','value' => (old('catalog_name') ? old('catalog_name') : $cataloghd->catalog_name),'attribute' => 'required'])!!}
      {!!view($view_path.'.builder.textarea',['name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $cataloghd->description),'attribute' => 'required'])!!}
      <div class="form-group">
        <label for="tag">Store</label>
        <select class="select2" name="store_id[]" multiple="multiple">
          @if(!is_array(json_decode(auth()->guard($guard)->user()->store_id)) && in_array(auth()->guard($guard)->user()->store_id,[0,1]))
            <option value="0" {{old('store_id') ? (in_array(0,old('store_id')) ? 'selected' : '') : ''}}>-- All Store --</option>
          @endif
          @foreach($store as $s)
            <option value="{{$s->id}}" {{old('store_id') ? (in_array($s->store_id,old('store_id')) ? 'selected' : '') : in_array($s->id,$catalog_store) ? 'selected' : ''}}>{{$s->store_name}}</option>
          @endforeach
        </select>
      </div>
      {!!view($view_path.'.builder.text',['name' => 'end_date','label' => 'Valid Until','value' => (old('end_date') ? old('end_date') : date_format(date_create($cataloghd->end_date),'d-m-Y')),'class' => 'datepicker','attribute' => 'required readonly'])!!}
      <div class="row">
        <div class="col-md-12">
          @foreach($catalogdt as $c)
            <div class="col-md-3 list-image-catalog">
              <img src="{{asset($image_path)}}/{{$cataloghd->id}}/{{$c->image}}" class="image-catalog">
              <input type="hidden" name="id[]" value="{{$c->id}}">
              <div class="row action">
                <div class="text-center"><i class="{{$c->status_primary == 'y' ? 'fa fa-check text-success' : 'fa fa-times text-danger'}} primary-image"></i></div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</form>
<div class="modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="image-modal-title">Ubah Catalog #<span class="catalog-id"></span></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="catalog_dt_id">
        {!!view($view_path.'.builder.text',['name' => 'catalog_dt_name','label' => 'Catalog Name','value' => '','class' => 'catalog_dt_name'])!!}
        {!!view($view_path.'.builder.textarea',['name' => 'catalog_dt_description','label' => 'Catalog Description','value' => '','class' => 'catalog_dt_description'])!!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary update-image">Save changes</button>
      </div>
    </div>
  </div>
</div>
@push('custom_css')
  <style type="text/css">
    input[type="file"]{
      display: block;
      width: 100%;
      height: 100%;
      opacity: 0;
    }
  </style>
@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,textarea').prop('disabled',true);
    });
  </script>
@endpush
@endsection
