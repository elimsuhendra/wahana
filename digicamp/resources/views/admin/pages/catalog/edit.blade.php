@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$cataloghd->id}}">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green portlet-container">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
        <div class="head-button">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint"><i class="fa fa-arrow-left"></i> {{trans('general.back')}}</button></a>
          {!!view($view_path.'.builder.button',['type' => 'submit','label' => 'Simpan'])!!}
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      {!!view($view_path.'.builder.text',['name' => 'catalog_name','label' => 'Catalog Name','value' => (old('catalog_name') ? old('catalog_name') : $cataloghd->catalog_name),'attribute' => 'required'])!!}
      {!!view($view_path.'.builder.textarea',['name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $cataloghd->description),'attribute' => 'required'])!!}
      <div class="form-group">
        <label for="tag">Store</label>
        <select class="select2" name="store_id[]" multiple="multiple">
          @if(!is_array(json_decode(auth()->guard($guard)->user()->store_id)) && in_array(auth()->guard($guard)->user()->store_id,[0,1]))
            <option value="0" {{old('store_id') ? (in_array(0,old('store_id')) ? 'selected' : '') : ''}}>-- All Store --</option>
          @endif
          @foreach($store as $s)
            <option value="{{$s->id}}" {{old('store_id') ? (in_array($s->store_id,old('store_id')) ? 'selected' : '') : in_array($s->id,$catalog_store) ? 'selected' : ''}}>{{$s->store_name}}</option>
          @endforeach
        </select>
      </div>
      {!!view($view_path.'.builder.text',['name' => 'end_date','label' => 'Valid Until','value' => (old('end_date') ? old('end_date') : date_format(date_create($cataloghd->end_date),'d-m-Y')),'class' => 'datepicker','attribute' => 'required readonly'])!!}
      <div class="status-upload text-center text-info" style="display:none;">
        Uploading 
        <span class="current-upload"></span>
        /
        <span class="total-upload"></span>
      </div>
      <div class="drop-zone-image">
        <div class="information">
          <div class="name">
            Upload Image Here  <i class="fa fa-upload"></i>
          </div>
          <small>* You can drag multiple image here</small>
        </div>
        <input type="file" class="upload-image" multiple>
      </div>
      <hr/>
      <div class="wrapper-image-catalog">
        <div class="row sortable container-image-catalog">
          @foreach($catalogdt as $c)
            <div class="col-md-3 list-image-catalog">
              <img src="{{asset($image_path)}}/{{$cataloghd->id}}/{{$c->image}}" class="image-catalog">
              <input type="hidden" name="id[]" value="{{$c->id}}">
              <div class="row action">
                <div class="col-xs-4"><i class="fa fa-pencil text-success edit-image" data-id="{{$c->id}}"></i></div>
                <div class="col-xs-4"><i class="{{$c->status_primary == 'y' ? 'fa fa-check text-success' : 'fa fa-times text-danger'}} primary-image" data-id="{{$c->id}}"></i></div>
                <div class="col-xs-4"><i class="fa fa-trash text-danger delete-image" data-id="{{$c->id}}"></i></div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</form>
<div class="modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="image-modal-title">Ubah Catalog #<span class="catalog-id"></span></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="catalog_dt_id">
        {!!view($view_path.'.builder.text',['name' => 'catalog_dt_name','label' => 'Catalog Name','value' => '','class' => 'catalog_dt_name'])!!}
        {!!view($view_path.'.builder.textarea',['name' => 'catalog_dt_description','label' => 'Catalog Description','value' => '','class' => 'catalog_dt_description'])!!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary update-image">Save changes</button>
      </div>
    </div>
  </div>
</div>
@push('custom_css')
  <style type="text/css">
    input[type="file"]{
      display: block;
      width: 100%;
      height: 100%;
      opacity: 0;
    }
  </style>
@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      @if ($role->view == 'n')
        $('input,select,textarea').prop('disabled',true);
      @else
        $(document).on('mouseenter drag','.drop-zone-image',function(){
          $(this).addClass('hover');
        });

        $(document).on('mouseleave mouseup','.drop-zone-image',function(){
          $(this).removeClass('hover');
        });

        $(document).on('change','.upload-image',function(e){
          var image           = e.target.files;
          var current_upload  = 0;
          var total_upload    = 0;
          var finishUpload    = false;
          $.each(image,function(i,v){
            var type  = v.type.split('/')[0];
            if(type != 'image'){
              $.growl_alert('All files type must be image','danger');
              $('.upload-image').val(null);
              finishUpload  = true;
              return false;
            }
          });

          if(finishUpload == true){
            return;
          }

          $('.status-upload').fadeIn();
          current_upload      = 0;
          total_upload        = image.length;
          $('.current-upload').html(current_upload);
          $('.total-upload').html(total_upload);
          $.each(image,function(i,v){
            var formdata  = new FormData();
            formdata.append('image',v);
            formdata.append('id','{{$cataloghd->id}}');
            $.postformdata('{{url($path)}}/ext/upload_image',formdata).success(function(data){
              current_upload  += 1;
              $('.current-upload').html(current_upload);
              if(current_upload == total_upload){
                $('.wrapper-image-catalog').load('{{url()->current()}} .container-image-catalog',function(){
                  $.sortable();
                  $('.upload-image').val(null);
                  setTimeout(function(){
                    $('.status-upload').fadeOut();
                  },2000);
                });
              }
            });
          });
        });
        $(document).on('click','.edit-image',function(){
          var id    = $(this).data('id');
          var data  = {id:id};
          $('.catalog-id').html(id);
          $('.catalog_dt_description').html('');
          $.postdata('{{url($path)}}/ext/edit_image',data).success(function(data){
            $('#image-modal').modal('show');
            $('.catalog_dt_id').val(data.catalog.id);
            $('.catalog_dt_name').val(data.catalog.catalog_name);
            $('.catalog_dt_description').val(data.catalog.description);
          });
        })
        $(document).on('click','.update-image',function(){
          var id            = $('.catalog_dt_id').val();
          var catalog_name  = $('.catalog_dt_name').val();
          var description   = $('.catalog_dt_description').val();
          var data  = {id:id,catalog_name:catalog_name,description:description};
          $.postdata('{{url($path)}}/ext/update_image',data).success(function(data){
            $.growl_alert('Data updated','success');
          });
        })
        $(document).on('click','.delete-image',function(){
          var self  = $(this);
          var id    = self.data('id');
          var data  = {id:id};
          $.postdata('{{url($path)}}/ext/delete_image',data).success(function(data){
            $('.wrapper-image-catalog').load('{{url()->current()}} .container-image-catalog',function(){
              $.sortable();
            });
          });
        })
        $(document).on('click','.primary-image',function(){
          var self  = $(this);
          var id    = self.data('id');
          var data  = {id:id};
          $.postdata('{{url($path)}}/ext/primary_image',data).success(function(data){
            $('.wrapper-image-catalog').load('{{url()->current()}} .container-image-catalog',function(){
              $.sortable();
            });
          });
        })
      @endif
    });
  </script>
@endpush
@endsection
