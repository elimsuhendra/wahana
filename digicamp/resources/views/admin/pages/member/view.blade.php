@extends($view_path.'.layouts.master')
@push('css')
	<link href="{{asset('components/back/css/pages/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Overview </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                @if($member->image != null)
                                    <img src="{{asset('components/admin/image/member/'.$member->id.'/'.$member->image)}}" class="img-responsive pic-bordered" alt="" />
                                @else
                                    <img src="{{asset('components/admin/image/default.jpg')}}" class="img-responsive pic-bordered" alt="" />
                                @endif
                            </li>
                            <!-- <li>
                                <a href="javascript:;"> Messages
                                    <span> 3 </span>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h1 class="font-green sbold uppercase">{{$member->username}}</h1>
                                <ul class="list-inline">
                                    <li>
                                        <i class="fa fa-calendar"></i> {{date( 'd M Y', strtotime($member->created_at) )}} </li>
                                    <li>
                                        <i class="fa fa-envelope"></i> {{$member->email}} </li>
                                    <li>
                                        <i class="fa fa-phone"></i> {{$member->phone != "" ? $member->phone : "Not filled"}} </li>
                                </ul>
                            </div>
                            <!--end col-md-8-->
                            <!--
                            <div class="col-md-4">
                                <div class="portlet sale-summary">
                                    <div class="portlet-title">
                                        <div class="caption font-red sbold"> Point Summary </div>
                                        <div class="tools">
                                            <a class="reload" href="javascript:;"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <ul class="list-unstyled">
                                        	@foreach($member_point as $mp)
                                            <li>
                                                <span class="sale-info"> {{$mp->store->store_name}}
                                                    <i class="fa fa-img-up"></i>
                                                </span>
                                                <span class="sale-num"> {{$mp->point}} </span>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> Summary </a>
                                </li>
                                <li>
                                    <a href="#tab_1_12" data-toggle="tab"> Transaction Log </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="tab_1_12">
                                    <div class="portlet-body">
                                        @if(count($invoice_log) > 0)
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-building"></i> Store </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-user"></i> User </th>
                                                    <th class="text-right">
                                                        <i class="fa fa-tag"></i> Transaction </th>
                                                    <th class="text-right">
                                                        <i class="fa fa-star"></i> Point </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	@foreach($invoice_log as $il)
                                                <tr>
                                                    <td>
                                                        {{$il->store->store_name}}
                                                    </td>
                                                    <td class="hidden-xs"> {{$il->member_user->name}} </td>
                                                    <td class="text-right"> 
                                                    	{{number_format($il->total_price)}}
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($il->point)}}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @endif
                                    </div>
                                </div>

                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        @if(count($member_point) > 0)
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-building"></i> Store </th>
                                                     <th>
                                                        <i class="fa fa-credit-card"></i> ID </th>
                                                    <th class="text-right">
                                                        <i class="fa fa-star"></i> Point </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($member_point as $mp)
                                                <tr>
                                                    <td>
                                                        {{$mp->store->store_name}}
                                                    </td>
                                                    <td>
                                                        {{$mp->member_number}}
                                                    </td>
                                                    <td class="text-right">
                                                        {{number_format($mp->point)}}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>
@endsection