$(document).ready(function(){
  //Config
  $( document ).ajaxStart(function() {
    $( "#overlay" ).show();
  });
   $( document ).ajaxStop(function() {
    $( "#overlay" ).hide();  
  });
  $.ajaxSetup({
    beforeSend: function( xhr ) {
       xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
    }
  });

  //Function / Helper
  $.extend({
  	root:function(){
  		return $('meta[name="root_url"]').attr('content');
  	},postformdata:function(url,formdata){
      data = $.ajax({
              url: url,
              type: "POST",
              data: formdata,
              contentType: false, 
              cache: false,
              processData:false,
          });
          return data;
    },postdata:function(url,formdata){
      data = $.ajax({
              url: url,
              type: "POST",
              data: formdata
          });
          return data;
    },getdata:function(url){
      data = $.ajax({
              url: url,
              type: "GET"
          });
          return data;
    },validemail:function(email){
      var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
          return pattern.test(email);
    },settimeoutalert:function(){
      window.setTimeout(function() {
          $(".alert alert-remove").fadeTo(1500, 0).slideUp(500, function(){
            $(this).remove(); 
          });
      }, 2000);
    },base64image:function(inputElement){
      var deferred = $.Deferred();
      var files = inputElement;
      if (files && files[0]) {
          var fr= new FileReader();
          fr.onload = function(e) {
              deferred.resolve(e.target.result);
          };
          fr.readAsDataURL( files[0] );
      } else {
          deferred.resolve(undefined);
      }

      return deferred.promise();

    },growl:function(){

      grw = $('.growl-alert');

      if(grw.length > 0){

        $.bootstrapGrowl(grw.data('message'), {

          ele: 'body', // which element to append to

          type: grw.data('type'), // (null, 'info', 'danger', 'success', 'warning')

          offset: {

              from: 'top',

              amount: 100

          }, // 'top', or 'bottom'

          align: 'right', // ('left', 'right', or 'center')

          width: 250, // (integer, or 'auto')

          delay: 5000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!

          allow_dismiss: true, // If true then will display a cross to close the popup.

          stackup_spacing: 10 // spacing between consecutively stacked growls.

        });

      }

    }, select2:function(){
      $(".edp_city").select2({
        placeholder: "-- Pilih Pilihan --"
      });

    },growl_alert:function(text,type){

      $.bootstrapGrowl(text, {

        ele: 'body', // which element to append to

        type: type, // (null, 'info', 'danger', 'success', 'warning')

        offset: {

            from: 'top',

            amount: 100

        }, // 'top', or 'bottom'

        align: 'right', // ('left', 'right', or 'center')

        width: 250, // (integer, or 'auto')

        delay: 5000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!

        allow_dismiss: true, // If true then will display a cross to close the popup.

        stackup_spacing: 10 // spacing between consecutively stacked growls.

      });

      $.settimeoutalert();

    }
  });

  $.growl();
  $.select2();

  //Core Jquery
  var miniCartContainer   = $('.open-cart-popup');
  var miniCart            = $('.cart-box > .popup-container > .mini-cart');
  var totalCart           = $('.total-cart');
  var cartParts           = $('.cart-parts');
  var CardData            = $('.crd_data');
  var needLoad            = 0;
 
  $.extend({
    coreCart:function(){
      $(document).on('click','.add-cart',function(){
        that            = this;
        var productAttr = $('.product-attr.active');
        var productQty  = $('.product-qty');
        var productId   = $(that).data('id');
        var qty         = productQty.text();
        var attr        = [];

        productAttr.each(function(i,v){
          var attrId  = $(this).data('id');
          attr.push(attrId);
        });
        var data        = {'id':productId,'attr':attr,'qty':qty};
        $(that).attr('disabled',true);
        $.postdata($.root()+'v1/add-cart',data).success(function(res){
          if(res.result.status == 'available'){
            $.emptyMiniCart();
            $.setTotalCart();
            $("html, body").animate({ scrollTop: 0 }, 600);
            setTimeout(function(){
              $(miniCartContainer).trigger('mouseover');
            },1000);
          }
          $.growl_alert(res.result.text,res.result.type);
          $(that).attr('disabled',false);
        });
      });

      $(document).on('click','.remove-mini-cart',function(){
        var id    = $(this).data('id');
        var data  = {'id' : id};
        $.postdata($.root()+'v1/remove-cart',data).success(function(res){
          $.emptyMiniCart();
          $.setTotalCart();
        });
      });

      $(document).on('change','.single-image',function(res){
        if(res){
          name_method = $(this).attr('name');
          files       = this.files;
          ext         = files[0].name.split('.')[1].toLowerCase();
          size        = files[0].size;
          allow_ext   = ['jpg','gif','png','jpeg'];
          if($.inArray(ext,allow_ext) > -1){
            if(size <= 2000000){
              $.base64image(files).done(function(res){
                file = "<img src='"+res+"' class='img-responsive img_center' width='100px' height='100px'>";
                $('input[name="remove-single-image-'+name_method+'"]').val('n');
                $('.single-image-'+name_method).html(file);
              });
            }else{
              alert("File size is to large");
            }
          }else{
            file = "<img src='"+$.root()+"components/both/images/web/none.png' class='img-responsive img_center' width='100px' height='100px'>";
            $(this).val(null);
            $('.single-image-'+name_method).html(file);
            alert ("File must image");
          }
        }
      });

      $(document).on('click','.remove-single-image',function(){
        a = $(this).data('id');
        b = $(this).data('name');
        $('input[name="remove-'+a+'-'+b+'"]').val('y');
        $('.'+a+'-'+b+' > img').attr('src',$.root()+'components/both/images/web/none.png');
        $('input[name="'+b+'"]').val(null);
      });

      $(document).on('click','.ep_crd_bt',function(){
        no_crd = $('#ep_card').val();

        var data  = {'id' : no_crd};

        $('#loader-wrapper').fadeIn();

        if(no_crd != ""){
          $.postdata($.root()+'add-card',data).success(function(res){
            var respon = res.status;
            var message = res.message;

            if(respon == 'success'){
              $.cardParts();     
              $('.st_success').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>'+ message +'</p></div>');
            } else if(respon == 'failed'){
               $.cardParts();     
               $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>'+ message +'</p></div>');
            }

            $('#loader-wrapper').fadeOut();
          }).error(function(res){
            $('#loader-wrapper').fadeOut();
            $.growl_alert("Sorry, there a problem with your request");
          });
        } else{
          $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Card Number Required</p></div>');
          $('#loader-wrapper').fadeOut();
        }
      });

      $(document).on('click','.vrg_sent',function(){
        email = $('#em_text').val();
        var data  = {'email' : email};

        $('#loader-wrapper').fadeIn();

        if(email != ""){
          $.postdata($.root()+'sent-verif',data).success(function(res){
            var respon = res.status;

            if(respon == 'success'){
              $('.st_success').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Verification Code Sent Success</p></div>');
            } else if(respon == 'failed'){
              $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>There a Problem, Please try again</p></div>');
            } else if(respon == "invalid"){
              $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Sorry, Email not valid</p></div>');                            
            }

            $('#loader-wrapper').fadeOut();

          }).error(function(res){
            $('#loader-wrapper').fadeOut();
            $.growl_alert("Sorry, there a problem with your request");
          });
        } else{
          $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Email Required</p></div>');
          $('.loader-custom').fadeOut();
        }
      });

      $(document).on('click','.vrf_sent',function(){
        email = $('#em_dta').val();
        var data  = {'email' : email};

        $('#loader-wrapper').fadeIn();

        if(email != ""){
          $.postdata($.root()+'sent-verif',data).success(function(res){
            var respon = res.status;

            if(respon == 'success'){
              $('.st_success').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Verification Code Sent Success</p></div>');
            } else if(respon == 'failed'){
              $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>There a Problem, Please try again</p></div>');
            } else if(respon == "invalid"){
              $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Sorry, Email not valid</p></div>');                            
            }

            $('#loader-wrapper').fadeOut();

          }).error(function(res){
            $('#loader-wrapper').fadeOut();
            $.growl_alert("Sorry, there a problem with your request");
          });
        } else{
          $('#loader-wrapper').fadeOut();
          $('.st_success').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Email Required</p></div>');
        }
      });

      $(document).on('keyup', '#hd_search', function(){
        var val = $('#hd_search').val();
        var data  = {search:val};
        var load = '';

        if(val.length >= 1) {
          load = '<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>';
          $('.rs_search').css("display", "block");
          $('.rs_search').html(load);

          $.postdata($.root()+'v1/live_search',data).success(function(res){
            data = res.result;
            var tmp = '';

            if(res.result.length >= 1) {
              data.forEach(function(v,i){
                tmp += '<p class="rs_search_p" id="s_'+ i +'" onclick="search_function('+ i +')">'+v.category_name+'</p>';
              }); 
            } else {
              tmp = '<p>No Result</p>';
            }

            $('.bubblingG').fadeOut();

            $('.rs_search').css("display", "block");
            $('.rs_search').html(tmp);
          }).error(function(res){
            tmp = '<p>Sorry, there a problem with your request.</p>'
            $('.rs_search').css("display", "block");
            $('.rs_search').html(tmp);
          });
        }
      });

      $(document).on('change','.get-district',function(e,ret){
        var target  = $(this).data('target');
        var id      = $(this).val();
        var temp    = '<option value="">-- Pilih Kecamatan --</option>'
        var data    = {id:id};

        $('#loader-wrapper').fadeIn();

        $.postdata($.root()+'v1/get-district',data).success(function(res){

          var district  = res.district;
          district.forEach(function(v,i){
            temp      += '<option value="'+v.id+'">'+v.name+'</option>';
          });

          $('.'+target).html(temp);
          $('#loader-wrapper').fadeOut();

        }).error(function(res){
          $('#loader-wrapper').fadeOut();
          $.growl_alert("Sorry, there a problem with your request");
        });

      });

      $(document).on('change','.district',function(e,ret){
        var target  = $(this).data('target');
        var id      = $(this).val();
        var dis     = $('.district').val();
        var temp    = '<option value="">-- Pilih Kelurahan --</option>'
        var data    = {id:id};

        $('#loader-wrapper').fadeIn();

        $.postdata($.root()+'v1/get-subdistrict',data).success(function(res){
          var subdistrict  = res.subdistrict;
          subdistrict.forEach(function(v,i){
            temp      += '<option value="'+v.id+'">'+v.name+'</option>';
          });

          $('.'+target).html(temp);
          $('#loader-wrapper').fadeOut();
        }).error(function(res){
          $('#loader-wrapper').fadeOut();
          $.growl_alert("Sorry, there a problem with your request");
        });
      });

      $(document).on('change','.subdistrict',function(e,ret){
        var target  = $(this).data('target');
        var id      = $(this).val();
        var dis     = $('.district').val();
        var temp    = '<option value="">-- Pilih Kodepos --</option>'
        var data    = {'id':id,'dis':dis};

        $('#loader-wrapper').fadeIn();

        $.postdata($.root()+'v1/get-postcode',data).success(function(res){
          var postcode  = res.postcode;
          postcode.forEach(function(v,i){
            temp      += '<option value="'+v.id+'">'+v.postcode+'</option>';
          });

          $('.'+target).html(temp);
          $('#loader-wrapper').fadeOut();

        }).error(function(res){
          $('#loader-wrapper').fadeOut();
          $.growl_alert("Sorry, there a problem with your request");
        });

      });

      $(document).mouseup(function (e)
      {
        var container = $(".rs_search");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
      });



      $(document).on('click','.add-wishlist',function(){
        $.growl_alert('Product added to wishlist','success');
      });

      if($(CardData).length > 0){
           $.cardParts();     
      }

      if($('.cart-box').length > 0){        
        $(document).on('mouseover',miniCartContainer,function(){
          if($(miniCart).is(':empty')){
            needLoad  += 1;
          }
          if(needLoad == 1){
            $('.mini-cart-loader').show();
            setTimeout(function(){
              $.miniCart();
            },1000);
          }
        });
      }
    },miniCart:function(){
      $.getdata($.root()+'parts/mini-cart').success(function(res){
        $(miniCart).html(res);
        needLoad  = 0;
        $('.mini-cart-loader').hide();
      }).error(function(err){
        needLoad  = 0;
      });
    },emptyMiniCart:function(){
      $(miniCart).html('');
      needLoad  = 0;
    },cardParts:function(){
      $( "#overlay" ).show();

      $.getdata($.root()+'parts/cards').success(function(res){
        CardData.html(res);
        $( "#overlay" ).hide();  
      });
    },setTotalCart:function(){
      $.getdata($.root()+'v1/total-cart').success(function(res){
        $(totalCart).html(res);
        $.miniCart();
      });
    },cartParts:function(){
      $.getdata($.root()+'parts/cart').success(function(res){
        cartParts.html(res);
        $('.cart-loader').fadeOut();
      });
    }
  })
  $.coreCart();
  // setTimeout(function(){
  //   $.setTotalCart();
  //   if($(cartParts).length > 0){
  //     $.cartParts();
  //   }
  // },1500);
});